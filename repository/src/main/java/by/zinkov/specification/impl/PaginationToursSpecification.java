package by.zinkov.specification.impl;

import by.zinkov.bean.Tour;
import by.zinkov.specification.PaginationSpecification;
import org.springframework.stereotype.Component;

@Component
public class PaginationToursSpecification extends PaginationSpecification<Tour> {

    public PaginationToursSpecification() {
        this.entityClass = Tour.class;
    }
}
