package by.zinkov.specification.impl;

import by.zinkov.bean.User;
import by.zinkov.specification.GetAllSpecification;
import org.springframework.stereotype.Component;

@Component
public class AllUsersSpecification extends GetAllSpecification<User> {
	public AllUsersSpecification() {
		entityClass = User.class;
	}
}
