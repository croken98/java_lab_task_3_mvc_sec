package by.zinkov.specification.impl;

import by.zinkov.bean.Hotel;
import by.zinkov.specification.PaginationSpecification;
import org.springframework.stereotype.Component;

@Component
public class PaginationHotelsSpecifications extends PaginationSpecification<Hotel> {

    public PaginationHotelsSpecifications() {
        this.entityClass = Hotel.class;
    }
}
