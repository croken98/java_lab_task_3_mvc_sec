package by.zinkov.specification.impl;

import by.zinkov.bean.Review;
import by.zinkov.specification.GetAllSpecification;
import org.springframework.stereotype.Component;

@Component
public class AllReviewsSpecification extends GetAllSpecification<Review> {
    public AllReviewsSpecification() {
        entityClass = Review.class;
    }
}
