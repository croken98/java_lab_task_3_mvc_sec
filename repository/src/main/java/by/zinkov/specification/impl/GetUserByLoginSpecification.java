package by.zinkov.specification.impl;

import by.zinkov.bean.User;
import by.zinkov.specification.JPASpecification;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
@Scope("prototype")
public class GetUserByLoginSpecification extends JPASpecification<User> {

    @Setter
    private String login;
    @Override
    public List<User> execute() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> user = criteriaQuery.from(User.class);
        Predicate loginPredicate = criteriaBuilder.equal(user.get("login"), login);
        criteriaQuery.select(user).where(loginPredicate);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
