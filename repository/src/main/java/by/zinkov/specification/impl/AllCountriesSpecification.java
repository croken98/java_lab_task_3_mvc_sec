package by.zinkov.specification.impl;

import by.zinkov.bean.Country;
import by.zinkov.specification.GetAllSpecification;
import org.springframework.stereotype.Component;

@Component
public class AllCountriesSpecification extends GetAllSpecification<Country> {
    public AllCountriesSpecification() {
        entityClass = Country.class;
    }
}
