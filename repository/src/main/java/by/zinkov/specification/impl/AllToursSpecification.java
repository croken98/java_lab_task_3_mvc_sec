package by.zinkov.specification.impl;

import by.zinkov.bean.Tour;
import by.zinkov.specification.GetAllSpecification;
import org.springframework.stereotype.Component;

@Component
public class AllToursSpecification extends GetAllSpecification<Tour> {
    public AllToursSpecification() {
        entityClass = Tour.class;
    }
}
