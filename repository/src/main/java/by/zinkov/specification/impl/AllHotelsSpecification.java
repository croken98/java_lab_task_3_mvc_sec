package by.zinkov.specification.impl;

import by.zinkov.bean.Hotel;
import by.zinkov.specification.GetAllSpecification;
import org.springframework.stereotype.Component;

@Component
public class AllHotelsSpecification extends GetAllSpecification<Hotel> {
    public AllHotelsSpecification() {
    entityClass = Hotel.class;
    }
}
