package by.zinkov.specification.impl;

import by.zinkov.bean.Country;
import by.zinkov.specification.PaginationSpecification;
import org.springframework.stereotype.Component;

@Component
public class PaginationCountriesSpecification extends PaginationSpecification<Country> {
    public PaginationCountriesSpecification() {
        this.entityClass = Country.class;
    }
}
