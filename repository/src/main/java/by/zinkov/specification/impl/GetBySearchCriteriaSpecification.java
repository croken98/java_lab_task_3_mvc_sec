package by.zinkov.specification.impl;

import by.zinkov.bean.Country;
import by.zinkov.bean.Hotel;
import by.zinkov.bean.Tour;
import by.zinkov.specification.JPASpecification;
import by.zinkov.util.SearchCriteria;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Component
public class GetBySearchCriteriaSpecification extends JPASpecification<Tour> {
    private static final String COUNTRY_ENTITY_FIELD = "country";
    private static final String STARS_ENTITY_FIELD = "stars";
    private static final String COST_ENTITY_FIELD = "cost";
    private static final String HOTEL_ENTITY_FIELD = "hotel";
    private static final String DURATION_ENTITY_FIELD = "duration";
    private static final String DATE_ENTITY_FIELD = "date";

    @Setter
    private SearchCriteria searchCriteria;

    @Override
    public List<Tour> execute() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tour> criteriaQuery = criteriaBuilder.createQuery(Tour.class);
        Root<Tour> tour = criteriaQuery.from(Tour.class);
        Join<Tour, Country> country = tour.join(COUNTRY_ENTITY_FIELD);
        Join<Tour, Hotel> hotel = tour.join(HOTEL_ENTITY_FIELD);

        Predicate resultPredicate = buildResultPredicate(searchCriteria, criteriaBuilder, tour, hotel, country);
        if(resultPredicate !=null) {
            criteriaQuery.select(tour).where(resultPredicate);
        } else {
            criteriaQuery.select(tour);
        }
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    private Predicate initStarsPredicate(SearchCriteria searchCriteria, CriteriaBuilder criteriaBuilder, Join<Tour, Hotel> hotel) {
        Byte minStar = searchCriteria.getMin(STARS_ENTITY_FIELD, Byte.class);
        Byte maxStar = searchCriteria.getMax(STARS_ENTITY_FIELD, Byte.class);
        if (minStar != null && maxStar != null) {
            return criteriaBuilder.between(hotel.get(STARS_ENTITY_FIELD), minStar, maxStar);
        } else {
            return null;
        }
    }

    private Predicate initCostPredicate(SearchCriteria searchCriteria, CriteriaBuilder criteriaBuilder, Root<Tour> tour) {
        BigDecimal minCost = searchCriteria.getMin(COST_ENTITY_FIELD, BigDecimal.class);
        BigDecimal maxCost = searchCriteria.getMax(COST_ENTITY_FIELD, BigDecimal.class);
        if (minCost != null && maxCost != null) {
            return criteriaBuilder.between(tour.get(COST_ENTITY_FIELD), minCost, maxCost);
        } else {
            return null;
        }
    }

    private Predicate initDurationPredicate(SearchCriteria searchCriteria, CriteriaBuilder criteriaBuilder, Root<Tour> tour) {
        Long minDuration = searchCriteria.getMin(DURATION_ENTITY_FIELD, Long.class);
        Long maxDuration = searchCriteria.getMax(DURATION_ENTITY_FIELD, Long.class);
        if (minDuration != null && maxDuration != null) {
            return criteriaBuilder.between(tour.get(DURATION_ENTITY_FIELD), minDuration, maxDuration);
        } else {
            return null;
        }
    }

    private Predicate initDatePredicate(SearchCriteria searchCriteria, CriteriaBuilder criteriaBuilder, Root<Tour> tour) {
        Date minDate = searchCriteria.getMin(DATE_ENTITY_FIELD, Date.class);
        Date maxDate = searchCriteria.getMax(DATE_ENTITY_FIELD, Date.class);
        if (minDate != null && maxDate != null) {
            Predicate datePredicateGreater =  criteriaBuilder.greaterThan(tour.get(DATE_ENTITY_FIELD), minDate);
            Predicate datePredicateSmaller = criteriaBuilder.lessThan(tour.get(DATE_ENTITY_FIELD), maxDate);
            return  criteriaBuilder.and(datePredicateGreater,datePredicateSmaller);
        } else {
            return null;
        }
    }

    private Predicate buildResultPredicate(SearchCriteria searchCriteria, CriteriaBuilder criteriaBuilder, Root<Tour> tour, Join<Tour, Hotel> hotel, Join<Tour, Country> country) {
        List<Predicate> predicates = new ArrayList();
        predicates.add(initStarsPredicate(searchCriteria, criteriaBuilder, hotel));
        predicates.add(initCostPredicate(searchCriteria, criteriaBuilder, tour));
        predicates.add(initDurationPredicate(searchCriteria, criteriaBuilder, tour));
        predicates.add(initDatePredicate(searchCriteria, criteriaBuilder, tour));
        List countries = searchCriteria.getConcreteValue(COUNTRY_ENTITY_FIELD, List.class);
        if (countries != null) {
            predicates.add(country.in(countries));
        }
        return predicates.
                stream().
                filter(Objects::nonNull).
                reduce(criteriaBuilder::and).orElse(null);
    }
}
