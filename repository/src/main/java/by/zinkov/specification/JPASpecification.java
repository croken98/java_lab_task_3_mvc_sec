package by.zinkov.specification;

import by.zinkov.repository.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public abstract class JPASpecification<T> implements Specification<T> {
    @PersistenceContext
    protected EntityManager entityManager;
}
