package by.zinkov.specification;

import by.zinkov.exception.PropertiesFileNotFoundException;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Properties;


@Component
public class PaginationSpecification<T> extends JPASpecification<T> {
    protected Class<T> entityClass;
    @Setter
    private int count;
    @Setter
    private int page;

    @Override
    public List<T> execute() {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Long> countQuery = criteriaBuilder
                .createQuery(Long.class);
        countQuery.select(criteriaBuilder
                .count(countQuery.from(entityClass)));
        Long allCount = entityManager.createQuery(countQuery)
                .getSingleResult();
        int offset = page * count;
        if (allCount > page * count) {
            CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
            Root<T> entity = criteriaQuery.from(entityClass);
            criteriaQuery.select(entity);
            TypedQuery<T> typedQuery = entityManager.createQuery(criteriaQuery);
            typedQuery.setFirstResult(offset);
            typedQuery.setMaxResults(count);
            return typedQuery.getResultList();
        } else {
            return Collections.emptyList();
        }
    }
}
