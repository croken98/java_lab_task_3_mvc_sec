package by.zinkov.specification;

import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Component("getAllSpecification")
public abstract class GetAllSpecification<T> extends JPASpecification<T> {
    protected Class<T> entityClass;

    @Override
    public List<T> execute() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        Root<T> entity = criteriaQuery.from(entityClass);
        criteriaQuery.select(entity);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
