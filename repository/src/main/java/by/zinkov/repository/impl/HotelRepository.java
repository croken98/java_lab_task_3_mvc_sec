package by.zinkov.repository.impl;


import by.zinkov.bean.Hotel;
import by.zinkov.repository.AbstractRepository;
import org.springframework.stereotype.Component;

@Component("hotelRepository")
public class HotelRepository extends AbstractRepository<Hotel,Integer> {

    @Override
    protected Class<Hotel> getEntityClass() {
        return Hotel.class;
    }
}
