package by.zinkov.repository.impl;

import by.zinkov.bean.Tour;
import by.zinkov.repository.AbstractRepository;
import org.springframework.stereotype.Component;

@Component("tourRepository")
public class TourRepository extends AbstractRepository<Tour,Integer> {

    @Override
    protected Class<Tour> getEntityClass() {
        return Tour.class;
    }
}
