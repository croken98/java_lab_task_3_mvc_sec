package by.zinkov.repository.impl;

import by.zinkov.bean.Review;
import by.zinkov.repository.AbstractRepository;
import org.springframework.stereotype.Component;

@Component("reviewRepository")
public class ReviewRepository extends AbstractRepository<Review,Integer> {

    @Override
    protected Class<Review> getEntityClass() {
        return Review.class;
    }
}
