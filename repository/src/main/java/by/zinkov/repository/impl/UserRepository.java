package by.zinkov.repository.impl;

import by.zinkov.bean.User;
import by.zinkov.repository.AbstractRepository;
import org.springframework.stereotype.Component;

@Component("userRepository")
public class UserRepository extends AbstractRepository<User,Integer> {
    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }
}
