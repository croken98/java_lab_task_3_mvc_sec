package by.zinkov.repository.impl;

import by.zinkov.bean.Country;
import by.zinkov.repository.AbstractRepository;
import org.springframework.stereotype.Component;

@Component("countryRepository")
public class CountryRepository extends AbstractRepository<Country,Integer> {
    @Override
    protected Class<Country> getEntityClass() {
        return Country.class;
    }
}
