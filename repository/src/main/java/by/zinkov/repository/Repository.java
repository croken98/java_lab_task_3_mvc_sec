package by.zinkov.repository;



import by.zinkov.bean.Identity;

import java.util.List;
import java.util.Optional;

public interface Repository<T extends Identity<PK>, PK> {
    Optional<T> save(T object);

    void delete(T object);

    void update(T object);

    Optional<T> getByPK(PK pk);

    long getCount();

    List<T> query(Specification<T> specification);
}
