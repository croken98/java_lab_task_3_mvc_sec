package by.zinkov.repository;

import java.util.List;

public interface Specification<T>{
    List<T> execute();
}
