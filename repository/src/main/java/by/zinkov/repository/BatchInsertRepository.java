package by.zinkov.repository;


import java.util.List;

public interface BatchInsertRepository<T> {
     void insertBatch(List<T> objects,int batchSize );
}
