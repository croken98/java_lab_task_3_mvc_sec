package by.zinkov.repository;

import by.zinkov.bean.Identity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class RepositoryFactory {
    @Autowired
    ApplicationContext applicationContext;

    public <T extends Identity<PK>, PK> Repository<T, PK> getRepository(Class<? extends Repository<T , PK>> beanClass) {
        return applicationContext.getBean(beanClass);
    }

}
