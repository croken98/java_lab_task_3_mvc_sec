package by.zinkov.repository;

import by.zinkov.bean.Identity;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Optional;

@Component
public abstract class AbstractRepository<T extends Identity<PK>, PK> implements Repository<T , PK>, BatchInsertRepository<T> {

    @PersistenceContext
    private EntityManager entityManager;

    protected abstract Class<T> getEntityClass();

    @Override
    public void insertBatch(List<T> objects, int batchSize) {
        for (int i = 0; i < objects.size(); i++) {
            if (i > 0 && i % batchSize == 0) {
                entityManager.flush();
                entityManager.clear();
            }
            entityManager.persist(objects.get(i));
        }
    }

    @Override
    public Optional<T> getByPK(PK pk) {
        return Optional.ofNullable(entityManager.find(getEntityClass(), pk));
    }

    @Override
    public Optional<T> save(T object) {
        entityManager.persist(object);
        entityManager.flush();
        return Optional.of(object);
    }

    @Override
    public void delete(T object) {
        object= entityManager.find(getEntityClass(), object.getId(), LockModeType.OPTIMISTIC);
        entityManager.remove(object);

    }

    @Override
    public void update(T object) {
        entityManager.merge(object);
        entityManager.flush();
    }

    @Override
    public List<T> query(Specification<T> specification) {
        return specification.execute();
    }

    @Override
    public long getCount() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Long> countQuery = criteriaBuilder
                .createQuery(Long.class);
        countQuery.select(criteriaBuilder
                .count(countQuery.from(getEntityClass())));
        return entityManager.createQuery(countQuery)
                .getSingleResult();
    }
}
