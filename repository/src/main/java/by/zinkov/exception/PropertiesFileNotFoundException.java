package by.zinkov.exception;

public class PropertiesFileNotFoundException extends RuntimeException {
    public PropertiesFileNotFoundException(Throwable cause) {
        super(cause);
    }
}
