package by.zinkov.security.detail;

import by.zinkov.bean.User;
import by.zinkov.repository.impl.UserRepository;
import by.zinkov.specification.impl.GetUserByLoginSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component("customDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    GetUserByLoginSpecification specification;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        specification.setLogin(username);
        List<User> userCondidate = userRepository.query(specification);
        Optional<User> userOptional = !userCondidate.isEmpty() ? Optional.of(userCondidate.get(0)) : Optional.empty();
//        if(userOptional.isPresent()){
//            return new UserDetailsImpl(userOptional.get());
//        } else throw new IllegalArgumentException("user not exist");
//
        return new UserDetailsImpl(
                userOptional.orElseThrow(IllegalArgumentException::new)
        );

    }
}
