package by.zinkov.controller;

import by.zinkov.dto.UserDto;
import by.zinkov.exception.LoginAlreadyExistException;
import by.zinkov.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegistrationController {
    private static final String REGISTRATION_URL = "/registration";
    @Autowired
    UserService userService;

    @RequestMapping(value = REGISTRATION_URL, method = RequestMethod.GET)
    public String showRegistrationForm(Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        return "registration";
    }

    @RequestMapping(value = REGISTRATION_URL, method = RequestMethod.POST)
    public ModelAndView registerUserAccount(
            @ModelAttribute("user") @Valid UserDto accountDto,
            BindingResult result, WebRequest request, Errors errors) throws LoginAlreadyExistException {
        UserDto registered = new UserDto();
        if (!result.hasErrors()) {
            registered = createUserAccount(accountDto);
        }
        if (registered == null) {
            result.rejectValue("login", "message.regError");
        }
        if (result.hasErrors()) {
            return new ModelAndView("registration", "user", accountDto);
        } else {
            return new ModelAndView("redirect:/login");
        }
    }

    private UserDto createUserAccount(UserDto accountDto) throws LoginAlreadyExistException{
            return userService.registerNewUserAccount(accountDto);
    }
}
