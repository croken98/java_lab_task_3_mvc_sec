package by.zinkov.controller;

import by.zinkov.bean.Review;
import by.zinkov.bean.Tour;
import by.zinkov.bean.User;
import by.zinkov.convertors.UserUserDtoConvertor;
import by.zinkov.dto.UserDto;
import by.zinkov.service.impl.ReviewService;
import by.zinkov.service.impl.TourService;
import by.zinkov.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

@Controller
public class ReviewController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserUserDtoConvertor converter;
    @Autowired
    private TourService tourService;
    @Autowired
    private ReviewService reviewService;

    @Secured("USER")
    @RequestMapping(value = "user/add_review", method = RequestMethod.POST)
    public String addReviewForUser(@Valid Review review, @RequestParam(value = "tour_id") int tourId, BindingResult result, Authentication authentication) {
        long currentDate = new Date().getTime();
        String currentUserName = authentication.getName();
        UserDto userDTO = userService.getByLogin(currentUserName);
        User user = converter.toEntity(userDTO);
        Tour tour = tourService.getByPk(tourId).orElseThrow(RuntimeException::new);

        review.setDate(new Timestamp(currentDate));
        review.setUser(user);
        review.setTour(tour);
        reviewService.add(review);
        return "redirect:/tours/" + tourId;
    }

    @RequestMapping(value = "/admin/remove_review", method = RequestMethod.POST)
    public String removeReview(@RequestParam("review_id") int reviewId, @RequestParam("tour_id") int tourId) {
        Optional<Review> reviewOptional = reviewService.getByPk(reviewId);
        reviewOptional.ifPresent((review -> reviewService.remove(review)));
        return "redirect:/tours/" + tourId;
    }
}
