package by.zinkov.controller;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ErrorController {
    private static final String DEFAULT_ERROR_VIEW = "errorPage";
    private static final String EXCEPTION_ATTRIBUTE = "exception";

    @ExceptionHandler(value = Exception.class)
    public ModelAndView
    defaultErrorHandler(Model req, Exception e) throws Exception {
        if (AnnotationUtils.findAnnotation
                (e.getClass(), ResponseStatus.class) != null) {
            throw e;
        }
        ModelAndView mav = new ModelAndView();
        mav.addObject(EXCEPTION_ATTRIBUTE, e.getMessage());
        mav.setViewName(DEFAULT_ERROR_VIEW);
        return mav;
    }
}