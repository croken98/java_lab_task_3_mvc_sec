package by.zinkov.controller;

import by.zinkov.bean.Tour;
import by.zinkov.convertors.UserUserDtoConvertor;
import by.zinkov.dto.UserDto;
import by.zinkov.security.detail.UserDetailsImpl;
import by.zinkov.service.impl.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class IndexController {
    @Autowired
    private UserUserDtoConvertor convertor;
    @Autowired
    private CountryService countryService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Authentication authentication, Model model, HttpSession session, @RequestParam(required = false) List<Tour> tours) {
        model.addAttribute("countries", countryService.getAll());
        if (authentication != null) {
            UserDetailsImpl details = (UserDetailsImpl) authentication.getPrincipal();
            UserDto userDto = convertor.toDTO(details.getUser());
            model.addAttribute("user", userDto);
            session.setAttribute("authentication", authentication);
        }
        return "index";
    }
}
