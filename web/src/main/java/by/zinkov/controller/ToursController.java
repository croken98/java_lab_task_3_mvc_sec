package by.zinkov.controller;

import by.zinkov.ControllerException;
import by.zinkov.bean.Review;
import by.zinkov.bean.Tour;
import by.zinkov.builder.SearchCriteriaBuilder;
import by.zinkov.convertors.UserUserDtoConvertor;
import by.zinkov.dto.UserDto;
import by.zinkov.form.FileForm;
import by.zinkov.json.impl.TourJSONParserFromFile;
import by.zinkov.service.impl.CountryService;
import by.zinkov.service.impl.TourService;

import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.util.*;

import by.zinkov.service.impl.UserService;
import by.zinkov.util.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;

@Controller
public class ToursController {
    private static final int DEFAULT_PAGE_NUMBER = 0;
    private static final int DEFAULT_COUNT_PER_PAGE = 12;

    @Autowired
    private UserService userService;
    @Autowired
    private TourService service;
    @Autowired
    private CountryService countryService;
    @Autowired
    private ApplicationContext context;
    @Autowired
    private UserUserDtoConvertor convertor;

    @RequestMapping(value = "/tours", method = RequestMethod.GET)
    public String getAllTours(
            @RequestParam(value = "page", required = false) Integer pageNumber
            , @RequestParam(value = "countPerPage", required = false) Integer countPerPage
            , Model model) {
        pageNumber = pageNumber != null ? pageNumber : DEFAULT_PAGE_NUMBER;
        countPerPage = countPerPage != null ? countPerPage : DEFAULT_COUNT_PER_PAGE;
        List<Tour> tours = service.getPaginatedTours(pageNumber, countPerPage);
        long toursCount = service.getCount();
        model.addAttribute("tours", tours);
        model.addAttribute("count", toursCount);
        model.addAttribute("page", pageNumber);
        model.addAttribute("countPerPage", countPerPage);
        return "tours";
    }

    @RequestMapping(value = "/tours/search", method = RequestMethod.POST)
    public String searchTours(
            @RequestParam Map<String, Object> allParams
            , @RequestParam(required = false) List<Integer> countriesId
            , RedirectAttributes redirectMap
    ) {

        SearchCriteriaBuilder builder = context.getBean(SearchCriteriaBuilder.class);
        allParams.put("countriesId", countriesId);
        SearchCriteria searchCriteria = null;
        try {
            searchCriteria = builder.build(allParams);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Tour> tours = service.getToursBySearchCriteria(searchCriteria);
        redirectMap.addFlashAttribute("tours", tours);
        return "redirect:/";
    }

    @Secured("ADMIN")
    @RequestMapping(value = "/admin/tours/upload", method = RequestMethod.GET)
    public String toDownloadToursPage(Model model) {
        model.addAttribute("fileForm", new FileForm());
        return "download_tours";
    }

    @Secured("ADMIN")
    @RequestMapping(value = "/admin/tours/upload", method = RequestMethod.POST)
    public String butchDownloadingTours(@Valid FileForm fileForm
            , BindingResult result, Model model) throws ControllerException {
        if (result.hasErrors()) {
            model.addAttribute("validationErrors", result.getAllErrors());
            return "download_tours";
        }
        MultipartFile file = fileForm.getFileTours();
        if (!file.isEmpty()) {
            TourJSONParserFromFile parser = new TourJSONParserFromFile();
            List<Tour> tours;
            try {
                Reader reader =new InputStreamReader(file.getInputStream());
                tours = parser.parse(reader);
                service.insertBath(tours);
            } catch (Exception e) {
                throw new ControllerException("Not valid file");
            }


        }
        return "redirect:/admin/tours/upload";
    }

    @RequestMapping(value = "/tours/{tour_id}", method = RequestMethod.GET)
    public ModelAndView toPersonalTour(@PathVariable(value = "tour_id") int tourId
            , ModelAndView modelAndView
            , Authentication authentication) {
        Optional<Tour> tourOptional = service.getByPk(tourId);
        if (authentication != null) {
            UserDto userDto = userService.getByLogin(authentication.getName());
            boolean hasTour = userService.hasThisTour(userDto, tourId);
            modelAndView.addObject("hasTour", hasTour);
        }
        Tour tour = tourOptional.orElseThrow(RuntimeException::new);
        List<Review> reviews = service.getTourReviews(tour);
        tour.setReviews(reviews);
        modelAndView.addObject("tour", tour);
        modelAndView.setViewName("tour");
        return modelAndView;
    }

    @Secured("ADMIN")
    @RequestMapping(value = "admin/tour/delete", method = RequestMethod.POST)
    public RedirectView deleteTour(
            @RequestParam int page
            , @RequestParam(value = "tour_id") int tourId
            , RedirectAttributes attributes) {
        Optional<Tour> tourOptional = service.getByPk(tourId);
        if (tourOptional.isPresent()) {
            service.remove(tourOptional.get());
        } else {
            attributes.addAttribute("error", "error.delete_tour_error");
        }
        attributes.addAttribute("page", page);
        return new RedirectView("/tours");
    }

    @Secured("USER")
    @RequestMapping(value = "user/add_tour", method = RequestMethod.POST)
    public String addTourForUser(
            @RequestParam("tour_id") int tourId
            , Authentication authentication) {
        Optional<Tour> tourOptional = service.getByPk(tourId);
        tourOptional.ifPresent((tour) -> {
            UserDto userDto = userService.getByLogin(authentication.getName());
            userService.addTour(userDto, tour);
        });
        return "redirect:/profile";
    }
}