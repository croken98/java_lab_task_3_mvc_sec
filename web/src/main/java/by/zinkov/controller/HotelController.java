package by.zinkov.controller;

import by.zinkov.bean.Features;
import by.zinkov.bean.Hotel;
import by.zinkov.service.impl.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class HotelController {
    private static final String HOTELS_URL = "/hotels";
    private static final String HOTELS_VIEW = "hotels";
    private static final String REDIRECT_TO_HOTELS_PAGE = "redirect:/hotels?page=";
    private static final int DEFAULT_PAGE_NUMBER = 0;
    private static final int DEFAULT_COUNT_PER_PAGE = 12;
    @Autowired
    HotelService service;

    @RequestMapping(value = HOTELS_URL, method = RequestMethod.GET)
    public String toHotelsPage(@RequestParam(value = "page", required = false) Integer pageNumber
            , @RequestParam(value = "countPerPage", required = false) Integer countPerPage
            , Model model) {
        pageNumber = pageNumber != null ? pageNumber : DEFAULT_PAGE_NUMBER;
        countPerPage = countPerPage != null ? countPerPage : DEFAULT_COUNT_PER_PAGE;
        List<Hotel> hotels = service.getPaginatedHotels(pageNumber, countPerPage);
        long toursCount = service.getCount();
        model.addAttribute("hotels", hotels);
        model.addAttribute("count", toursCount);
        model.addAttribute("page", pageNumber);
        model.addAttribute("features_array", Arrays.stream(Features.values()).collect(Collectors.toList()));
        model.addAttribute("countPerPage", countPerPage);
        return HOTELS_VIEW;
    }
    @Secured("ADMIN")
    @RequestMapping(value = "/admin/hotels/delete/", method = RequestMethod.POST)
    public String deleteHotel(@RequestParam("hotel_id") int hotelId, @RequestParam("page") int page, RedirectAttributes attributes) {
        List<String> errors = new ArrayList<>();
        Optional<Hotel> hotelOptional = service.getByPk(hotelId);
        if (hotelOptional.isPresent()) {
            service.remove(hotelOptional.get());
        } else {
            errors.add("error.delete_hotel");
        }
        attributes.addFlashAttribute("errorsList", errors);
        return REDIRECT_TO_HOTELS_PAGE + page;
    }

    @Secured("ADMIN")
    @RequestMapping(value = "/admin/hotels/add", method = RequestMethod.POST)
    public String addHotel(@Valid Hotel hotel, BindingResult result, @RequestParam("page") int page, RedirectAttributes attributes) {
        if (!result.hasErrors()) {
            service.add(hotel);
        } else {
            attributes.addFlashAttribute("validationRedirectErrors", result.getAllErrors());
        }
        return REDIRECT_TO_HOTELS_PAGE + page;
    }

    @Secured("ADMIN")
    @RequestMapping(value = "/admin/hotels/update", method = RequestMethod.POST)
    public String updateHotel(@Valid Hotel hotel, BindingResult result, @RequestParam("page") int page, RedirectAttributes attributes) {
        if (!result.hasErrors()) {
            service.update(hotel);
        } else {
            attributes.addFlashAttribute("validationRedirectErrors", result.getAllErrors());
        }
        return REDIRECT_TO_HOTELS_PAGE + page;
    }
}
