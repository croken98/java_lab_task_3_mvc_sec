package by.zinkov.controller;

import by.zinkov.bean.Review;
import by.zinkov.bean.Tour;
import by.zinkov.dto.UserDto;
import by.zinkov.service.impl.TourService;
import by.zinkov.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private TourService tourService;

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String toProfilePage(Authentication authentication, Model model) {
        UserDto userDto = userService.getByLogin(authentication.getName());
        List<Tour> tours = userService.getUserTours(userDto);
        List<Review> reviews = userService.getUserReviews(userDto);
        model.addAttribute("tours", tours);
        model.addAttribute("user", userDto);
        model.addAttribute("reviews", reviews);
        return "profile";
    }

    @Secured("USER")
    @RequestMapping(value = "/user/addTour", method = RequestMethod.POST)
    public String addTourToFavourites(
            @RequestParam("tour_id") int tourId
            , Authentication authentication) {
        UserDto userDto = userService.getByLogin(authentication.getName());
        Optional<Tour> tourOptional = tourService.getByPk(tourId);
        tourOptional.ifPresent(tour -> userService.addTour(userDto, tour));
        return "redirect:/tours/" + tourId;
    }

    @Secured("USER")
    @RequestMapping(value = "/user/removeTour", method = RequestMethod.POST)
    public String removeTourFromFavourites(
            @RequestParam("tour_id") int tourId
            , Authentication authentication) {
        UserDto userDto = userService.getByLogin(authentication.getName());
        userService.removeTourFromUser(userDto, tourId);
        return "redirect:/profile/";
    }
}
