package by.zinkov.builder;

import by.zinkov.bean.Country;
import by.zinkov.service.impl.CountryService;
import by.zinkov.util.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@Scope("prototype")
public class SearchCriteriaBuilder {
    private SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    private static final String COUNTRY_ENTITY_FIELD = "country";

    private static final String STARS_ENTITY_FIELD = "stars";
    private static final String MIN_STARS_ENTITY_FIELD = "minStars";
    private static final String MAX_STARS_ENTITY_FIELD = "maxStars";

    private static final String COST_ENTITY_FIELD = "cost";
    private static final String MIN_COST_ENTITY_FIELD = "minCost";
    private static final String MAX_COST_ENTITY_FIELD = "maxCost";

    private static final String DURATION_ENTITY_FIELD = "duration";
    private static final String MIN_DURATION_ENTITY_FIELD = "minDuration";
    private static final String MAX_DURATION_ENTITY_FIELD = "maxDuration";

    private static final String DATE_ENTITY_FIELD = "date";
    private static final String MIN_DATE_ENTITY_FIELD = "minDate";
    private static final String MAX_DATE_ENTITY_FIELD = "maxDate";

    @Autowired
    private CountryService countryService;

    private SearchCriteria searchCriteria = new SearchCriteria();

    public SearchCriteria build(Map<String, Object> allParams) throws ParseException {
        initCost(allParams);
        initStars(allParams);
        if (allParams.containsKey(COUNTRY_ENTITY_FIELD)) {
            buildHotels((List<Integer>) allParams.get(COUNTRY_ENTITY_FIELD));
        }
        initDuration(allParams);
        initDate(allParams);
        return searchCriteria;
    }

    private void initStars(Map<String, Object> allParams) {
        if (allParams.containsKey(MIN_STARS_ENTITY_FIELD)) {
            searchCriteria.setMin(STARS_ENTITY_FIELD, Byte.valueOf((String) allParams.get(MIN_STARS_ENTITY_FIELD)));
        }
        if (allParams.containsKey(MAX_STARS_ENTITY_FIELD)) {
            searchCriteria.setMax(STARS_ENTITY_FIELD, Byte.valueOf((String) allParams.get(MAX_STARS_ENTITY_FIELD)));
        }
    }

    private void initCost(Map<String, Object> allParams) {
        if (allParams.containsKey(MIN_COST_ENTITY_FIELD)) {
            searchCriteria.setMin(COST_ENTITY_FIELD, BigDecimal.valueOf(Double.valueOf((String) allParams.get(MIN_COST_ENTITY_FIELD))));
        }
        if (allParams.containsKey(MAX_COST_ENTITY_FIELD)) {
            searchCriteria.setMax(COST_ENTITY_FIELD, BigDecimal.valueOf(Double.valueOf((String) allParams.get(MAX_COST_ENTITY_FIELD))));
        }
    }

    private void initDuration(Map<String, Object> allParams) {
        if (allParams.containsKey(MIN_DURATION_ENTITY_FIELD)) {
            searchCriteria.setMin(DURATION_ENTITY_FIELD, Long.valueOf((String) allParams.get(MIN_DURATION_ENTITY_FIELD)));
        }
        if (allParams.containsKey(MAX_DURATION_ENTITY_FIELD)) {
            searchCriteria.setMax(DURATION_ENTITY_FIELD, Long.valueOf((String) allParams.get(MAX_DURATION_ENTITY_FIELD)));
        }
    }

    private void initDate(Map<String, Object> allParams) throws ParseException {
        if (allParams.containsKey(MIN_DATE_ENTITY_FIELD)) {
            searchCriteria.setMin(DATE_ENTITY_FIELD, formatter.parse((String) allParams.get(MIN_DATE_ENTITY_FIELD)));
        }
        if (allParams.containsKey(MAX_DATE_ENTITY_FIELD)) {
            searchCriteria.setMax(DATE_ENTITY_FIELD, formatter.parse((String) allParams.get(MAX_DATE_ENTITY_FIELD)));
        }
    }

    private void buildHotels(List<Integer> hotelsId) {
        List<Country> countries = new ArrayList<>();
        if (hotelsId == null) {
            return;
        }
        for (Integer idHotel : hotelsId) {
            Optional<Country> countryOptional = countryService.getByPk(idHotel);
            Country country;
            if (countryOptional.isPresent()) {
                country = countryOptional.get();
                countries.add(country);
            }
        }
        searchCriteria.setConcreteValue(COUNTRY_ENTITY_FIELD, countries);
    }
}
