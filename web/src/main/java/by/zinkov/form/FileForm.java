package by.zinkov.form;

import by.zinkov.validation.JSONFileConstraint;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class FileForm {
    @JSONFileConstraint
    private MultipartFile fileTours;
}
