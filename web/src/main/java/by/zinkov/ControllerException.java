package by.zinkov;

public class ControllerException extends Exception {

    public ControllerException(String message) {
        super(message);
    }
}
