package by.zinkov.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = JSONFileValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface JSONFileConstraint {
    String message() default "error.file_type.json";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}