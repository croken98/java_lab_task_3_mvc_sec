package by.zinkov.validation;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class JSONFileValidator implements
        ConstraintValidator<JSONFileConstraint, MultipartFile> {

    @Override
    public boolean isValid(MultipartFile value, ConstraintValidatorContext context) {
        String s = value.getOriginalFilename();
        return value.getOriginalFilename().matches("^.+\\.json");
    }

    @Override
    public void initialize(JSONFileConstraint constraintAnnotation) {

    }
}
