<#import "/spring.ftl" as spring />
<#import  "../macro/security.ftl" as security/>
<#setting number_format="computer">
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <script src="/public/uui/js/lib/jquery-1.12.0.min.js"></script>

    <link rel="stylesheet" href="/public/style/style.css"/>
    <link rel="stylesheet" href="/public/uui/bootstrap/css/bootstrap.min.css"/>
    <script src="/public/uui/bootstrap/js/bootstrap.min.js"></script>
    <script src="/public/uui/js/uui-core.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/public/uui/css/uui-all.css"/>
    <link rel="stylesheet" href="/public/uui/css/custom-styles.css"/>
    <link rel="stylesheet" href="/public/uui/css/lib/components/jquery.mCustomScrollbar.min.css"/>
    <script src="/public/uui/js/lib/components/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
<body>
<div class="wrapper">
    <header>
        <div class="uui-header navigation-header">
            <nav>
                <div class="uui-responsive-header lime-green">
                    <div class="responsive-header">
                        <div class="responsive-toggle-box">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div class="responsive-hide-menu">
                            <span></span>
                            <span></span>
                        </div>
                        <a href="<@spring.url '/'/>" class="responsive-brand-logo">
                            <span class="arrow fa fa-angle-left"></span>
                            <span class="logo">
                                  </span>
                            <span class="title">Travel Agency</span>
                        </a>
                    </div>
                    <div class="responsive-menu">
                        <div class="menu-wrapper">
                            <div class="menu-scroll">
                                <ul class="nav navbar-nav">
                                    <@security.ifAuthenticated>
                                        <li><a href="<@spring.url '/'/>profile"><span>Profile</span></a></li>
                                    </@security.ifAuthenticated>
                                    <li><a href="<@spring.url '/'/>hotels"><span>Hotels</span></a></li>
                                    <li><a href="<@spring.url '/'/>tours"><span>Tours</span></a></li>
                                    <@security.ifNotAuthenticated>
                                        <li><a href="<@spring.url '/'/>login"><span>LogIn</span></a></li>
                                        <li><a href="<@spring.url '/'/>registration"><span>SignUp</span></a></li>
                                    </@security.ifNotAuthenticated>
                                    <@security.ifAuthenticated>
                                        <li><a href="logout"><span>Logout</span></a></li>
                                    </@security.ifAuthenticated>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="/" class="brand-logo">
                          <span class="logo">
                          </span>
                    Travel Agency
                </a>
                <ul class="uui-navigation nav navbar-nav">
                    <@security.ifAuthenticated>
                        <li>
                            <a href="<@spring.url '/'/>profile"><span> <@spring.message code="profile"></@spring.message></span></a>
                        </li>
                    </@security.ifAuthenticated>
                    <li><a href="<@spring.url '/'/>hotels"><span><@spring.message code="hotels"/></span></a></li>
                    <li><a href="<@spring.url '/'/>tours"><span><@spring.message code="tours"/></span></a></li>
                    <@security.ifNotAuthenticated>
                        <li><a href="<@spring.url '/'/>login"><span><@spring.message code="login"/></span></a></li>
                        <li><a href="<@spring.url '/'/>registration"><span><@spring.message code="signup"/></span></a>
                        </li>
                    </@security.ifNotAuthenticated>
                    <@security.ifAuthenticated>
                        <li><a href="<@spring.url '/'/>logout"><span><@spring.message code="logout"/></span></a></li>
                    </@security.ifAuthenticated>
                    <@security.ifHasRole role="ADMIN">
                        <li>
                            <a href="<@spring.url '/'/>admin/tours/upload"><span><@spring.message code="upload_tours"/></span></a>
                        </li>
                    </@security.ifHasRole>
                </ul>
                <div class="links-style-header">
                    <a href="?mylocale=ru">русский</a> <span class="blue-custom">/</span>
                    <a href="?mylocale=en">english</a>
                </div>
            </nav>
        </div>
    </header>
    <div class="uui-main-container">
        <main>
            <#if errors ?? >
                <div class="uui-alert-messages fuchsia" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><span></span><span></span></span>
                    </button>
                    <i class="fa fa-exclamation-triangle"></i>
                    <strong><@spring.message code="warning"/></strong>
                    <#list errors as error>
                        <p> error </p> <br><br>
                    </#list>
                    <span class="sr-only"><@spring.message code="close"/></span>
                </div>
            </#if>

            <#if validationErrors ??>
                <div class="uui-alert-messages fuchsia" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><span></span><span></span></span>
                    </button>
                    <i class="fa fa-exclamation-triangle"></i>
                    <strong><@spring.message code="warning"/></strong>
                    <#list validationErrors as validationError>
                        <p> ${validationError.field}
                            : <@spring.message code="${validationError.defaultMessage}"></@spring.message> </p> <br><br>
                    </#list>
                    <span class="sr-only"><@spring.message code="close"/></span>
                </div>
            </#if>

            <#if validationRedirectErrors ??>
                <div class="uui-alert-messages fuchsia" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><span></span><span></span></span>
                    </button>
                    <i class="fa fa-exclamation-triangle"></i>
                    <strong>Cool!</strong>
                    <#list validationRedirectErrors as errorRedir>
                    <p> ${errorRedir.field}: ${errorRedir.defaultMessage}<br><br>
                        </#list>
                        <span class="sr-only"><@spring.message code="close"/></span>
                </div>
            </#if>
            <div class="margin-left-20px">


