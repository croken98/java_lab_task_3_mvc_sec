<#import "/spring.ftl" as spring />
<#include "frames/header.ftl">

<h2>Form Data Binding and Validation</h2>

<@spring.bind "user"/>
<#if user?? && noErrors??>
    Your submitted data<br>
    Login: ${user.login}<br>
    Password ${user.password}<br>
<#else>
    <form action="/registration" method="post">
        <@spring.message code="first_name"/>:<br>
        <@spring.formInput "user.login"/>
        <span class="error-msg-style" "><br><@spring.showErrors "<br>"/></span>
        <br><br>
        <@spring.message code="password"/><br>
        <@spring.formPasswordInput "user.password"/>
        <span class="error-msg-style"><br><@spring.showErrors "<br>"/></span>
        <br><br>
        <@spring.message code="repeat_password"/><br>
        <@spring.formPasswordInput "user.matchingPassword"/>
        <span class="error-msg-style"><br><@spring.showErrors "<br>"/></span>
        <br><br>
        <button><@spring.message code="signup"/></button>
    </form>
</#if>

<script src="/js/main.js"></script>
<#include "frames/footer.ftl">
