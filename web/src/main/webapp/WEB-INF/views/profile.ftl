<#include "frames/header.ftl">
<h1 class="uui-promo-heading margin-top-36px" ><@spring.message code="user"/></h1>

<div>
    <section class=" margin-top-36px">
        <ul class="uui-items-list">
            <li class="item-line-body">
                <div class="line">
                    <div class="wrapper">
                        <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check"><@spring.message code="login"/></i>
                    </span>
                        </div>
                        <div class="item-section content">
                            <p><span class="font-weight-bold">${user.login}</span></p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="item-line-body">
                <div class="line">
                    <div class="wrapper">
                        <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check"><@spring.message code="role"/></i>
                    </span>
                        </div>
                        <div class="item-section content">
                            <p><span class="font-weight-bold">${user.role}</span></p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </section>


    <h1 class="uui-promo-heading margin-top-36px" ><@spring.message code="tours"/></h1>
    <div class="col-lg-12 margin-bottom-72px">
        <#list tours as tour>
            <div class="col-lg-4">
                <section class="uui-image-text blue margin-top-36px">
                    <div class="image-section"
                         style="background-image: url(<@spring.url '/'/>public/photo/${tour.photo});"></div>
                    <div class="image-text-section">

                        <ul class="col-md-8 ptofile-contaier-tour" >
                            <p class="">type: <span class="font-weight-bold">${tour.tour}</span></p>
                            <p><@spring.message code="cost"/>: <span class="font-weight-bold">${tour.cost}</span></p>
                            <p><@spring.message code="duration"/>: <span class="font-weight-bold">${tour.duration}</span></p>
                            <p><@spring.message code="country"/>: <span class="font-weight-bold">${tour.country.name}</span></p>
                            <p><@spring.message code="date"/>: <span class="font-weight-bold">${tour.date}</span></p>
                        </ul>
                        <div class="col-md-4 profile-footer-containr">
                            <div class="margin-top-36px">
                                <form action="/user/removeTour" method="POST">
                                    <button class="uui-button" name="tour_id" value="${tour.id}">Remove</button>
                                </form>
                                <a href="<@spring.url '/'/>tours/${tour.id}/"
                                   class="uui-button lime-green right-icon margin-top-36px">
                                    <@spring.message code="more_info"/><i class="fa fa-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </#list>
    </div>
    <h1 class="uui-promo-heading margin-top-72px" ><@spring.message code="reviews"/></h1>

    <section class="uui-accordion" id="accordion1">
        <#list reviews as review>
            <div class="panel accordion-item">
                <div class="accordion-heading">
                    <p class="accordion-title">
                        <a data-toggle="collapse" data-parent="#accordion1" class="accordion-toggle"
                           href="<@spring.url '/'/>tours/${review.tour.id}">${review.date} </a>
                    </p>
                </div>
                <div class="accordion-collapse collapse in " id="item1">

                    <div class="accordion-body">
                        <a href="<@spring.url '/'/>tours/${review.tour.id}"
                        <p>${review.text}</p></a>
                    </div>
                </div>
            </div>
        </#list>
    </section>
    <#include "frames/footer.ftl">

