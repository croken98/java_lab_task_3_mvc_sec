<script src="https://api-maps.yandex.ru/2.1/?apikey=b9f00779-39b3-4da0-b8c3-becb9d63520e&lang=ru_RU"
        type="text/javascript"></script>
<#include "frames/header.ftl">
<#--<#assign security=JspTaglibs["myWebApp/META-INF/security.tld"] />-->
<h1 class="uui-promo-heading margin-top-36px" ><@spring.message code="tour"/></h1>
<@security.ifHasRole role="USER">
    <#if hasTour ??>
        <#if hasTour == false>
            <form method="post" action="/user/addTour">
                <input type="hidden" value="${tour.id}" name="tour_id">
                <button class="uui-button blue"><@spring.message code="add_to_favourite"/></button>
            </form>
        </#if>
    </#if>
</@security.ifHasRole>

<div>
    <section class="col-lg-3 margin-top-36px">
        <ul class="uui-items-list">
            <li class="item-line-body">
                <div class="line">
                    <div class="wrapper">
                        <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check">1</i>
                    </span>
                        </div>
                        <div class="item-section content">
                            <p><@spring.message code="type"/>: <span class="font-weight-bold"
                                        >${tour.tour.nameTour}</span></p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="item-line-body">
                <div class="line">
                    <div class="wrapper">
                        <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check">2</i>
                    </span>
                        </div>
                        <div class="item-section content">
                            <p><@spring.message code="cost"/>: <span class="font-weight-bold">${tour.cost}</span></p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="item-line-body">
                <div class="line">
                    <div class="wrapper">
                        <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check">3</i>
                    </span>
                        </div>
                        <div class="item-section content">
                            <p><@spring.message code="duration"/>: <span
                                        class="font-weight-bold">${tour.duration}</span></p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="item-line-body">
                <div class="line">
                    <div class="wrapper">
                        <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check">4</i>
                    </span>
                        </div>
                        <div class="item-section content">
                            <p><@spring.message code="country"/>: <span
                                        class="font-weight-bold">${tour.country.name}</span></p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="item-line-body">
                <div class="line">
                    <div class="wrapper">
                        <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check">5</i>
                    </span>
                        </div>
                        <div class="item-section content">
                            <p><@spring.message code="date"/>: <span class="font-weight-bold">${tour.date}</span></p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </section>
    <section class="col-lg-9">
        <img class="width100" src="<@spring.url '/'/>public/photo/${tour.photo}">
    </section>
    <h1 class="uui-promo-heading margin-top-36px" ><@spring.message code="hotel"/></h1>

    <section class="margin-bottom700px">
        <section class="col-lg-3 margin-top-36px">
            <ul class="uui-items-list">
                <li class="item-line-body">
                    <div class="line">
                        <div class="wrapper">
                            <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check">1</i>
                    </span>
                            </div>
                            <div class="item-section content">
                                <p><@spring.message code="name"/>: <span
                                            class="font-weight-bold">${tour.hotel.name}</span></p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="item-line-body">
                    <div class="line">
                        <div class="wrapper">
                            <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check">2</i>
                    </span>
                            </div>
                            <div class="item-section content">
                                <p><@spring.message code="website"/>: <span
                                            class="font-weight-bold">${tour.hotel.website}</span></p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="item-line-body">
                    <div class="line">
                        <div class="wrapper">
                            <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check">3</i>
                    </span>
                            </div>
                            <div class="item-section content">
                                <p><@spring.message code="raiting"/>: <span
                                            class="font-weight-bold">${tour.hotel.stars ? int} </span></p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="item-line-body">
                    <div class="line">
                        <div class="wrapper">
                            <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check">4</i>
                    </span>
                            </div>
                            <div class="item-section content">
                                <p class="margin-right-20"><@spring.message code="features"/>:</p>
                                <ul class="font-weight-bold margin-left-20px">
                                    <#list tour.hotel.features as feature>
                                        <li>${feature.nameFeature}</li>
                                    </#list>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="item-line-body">
                    <div class="line">
                        <div class="wrapper">
                            <div class="item-section icon">
                    <span class="uui-icon">
                        <i class="fa fa-check">5</i>
                    </span>
                            </div>
                            <div class="item-section content">
                                <p><@spring.message code="location"/>: <span class="uui-rating font-weight-bold" id="lastCordinates"
                                                                             ></span>

                            </div>
                        </div>
                    </div>
                </li>
            </ul>

        </section>
        <section class="col-lg-9">
            <div class="mapstyle" id="map" ></div>
        </section>
    </section>
    <h1 class="uui-promo-heading margin-top-36px" s><@spring.message code="reviews"/></h1>
    <div class="tour-width">
        <@security.ifAuthenticated>
            <form method="post" action="/user/add_review" class="tour-flex">
            <textarea name="text" placeholder="Input text"
                      class="form-tour-style"></textarea>
                <input type="hidden" value="${tour.id ? int}" name="tour_id">
                <button class="uui-button blue"><@spring.message code="send"/></button>
            </form>
        </@security.ifAuthenticated>
        <section class="uui-accordion" id="accordion1">

            <#list tour.reviews as review>
                <div class="panel accordion-item">
                    <div class="accordion-heading">
                        <p class="accordion-title">
                            <a data-toggle="collapse" data-parent="#accordion1" class="accordion-toggle"
                               href="#item1">${review.user.login} , ${review.date} </a>
                        </p>
                    </div>
                    <div class="accordion-collapse collapse in " id="item1">

                        <div class="accordion-body">
                            <p>${review.text}</p>
                            <@security.ifHasRole role="ADMIN">
                                <form method="post" action="/admin/remove_review">
                                    <input type="hidden" value="${tour.id}" name="tour_id">
                                    <input type="hidden" value="${review.id}" name="review_id">
                                    <button class="uui-button blue">Remove</button>
                                </form>
                            </@security.ifHasRole>
                        </div>
                    </div>
                </div>
            </#list>
        </section>
    </div>
</div>

<script src="/public/uui/js/uui-rating.min.js" type="text/javascript"></script>
<div class="margin-top-72px"></div>
<#include "frames/footer.ftl">
<script src="/public/js/maps.js"></script>
<script>
    initApp(("${tour.hotel.lalitude}" + "," + "${tour.hotel.longitude}").split(","))
</script>

