<#import "/spring.ftl" as spring />
<#include "frames/header.ftl">

<form method="POST" enctype="multipart/form-data"
      action="/admin/tours/upload">
    <@spring.message code="file.to.upload"></@spring.message>
    <input type="file" name="fileTours"><br/><br/>
    <input type="submit">
    <@spring.message code="file.buttom.upload"></@spring.message>
</form>
<#include "frames/footer.ftl">
