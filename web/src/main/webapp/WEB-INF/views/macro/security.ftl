<#macro ifAuthenticated>
    <#if Session.authentication ?? && Session.authentication.isAuthenticated()>
        <#nested>
    </#if>
</#macro>

<#macro ifNotAuthenticated>
    <#if !(Session.authentication ??) || !Session.authentication.isAuthenticated()>
        <#nested>
    </#if>
</#macro>

<#macro ifHasRole role>
    <#if Session.authentication ?? && Session.authentication.isAuthenticated()>
        <#if Session.authentication.getAuthorities()? seq_contains(role)>
            <#nested>
        </#if>
    </#if>
</#macro>