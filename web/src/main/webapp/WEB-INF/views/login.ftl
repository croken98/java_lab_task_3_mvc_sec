<#import "/spring.ftl" as spring />

<#include "frames/header.ftl">

<h2>Form Data Binding and Validation</h2>

<@spring.bind "user"/>
<#if error ??>
    <h2 class="color-red"> <@spring.message code="invalid_login"/></h2>
</#if>
<#if user?? && noErrors??>
    Your submitted data<br>
    Login: ${user.login}<br>
    Password ${user.password}<br>
<#else>
    <form action="/login" method="post">
        <@spring.message code="first_name"/>:<br>
        <@spring.formInput "user.login"/>
        <@spring.showErrors "<br>"/>
        <br><br>
        <@spring.message code="password"/><br>
        <@spring.formInput "user.password"/>
        <@spring.showErrors "<br>"/>
        <br><br>
        <@spring.message code="remember.me"/>:<br>

        <input type="checkbox" name="remember_me">
        <br><br>
        <button><@spring.message code="login"/></button>
    </form>
</#if>

<script src="/js/main.js"></script>
<#include "frames/footer.ftl">
