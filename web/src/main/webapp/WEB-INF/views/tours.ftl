<#include "frames/header.ftl">

<h1 class="uui-promo-heading tours-main-container" ><@spring.message code="tours"/></h1>
<#list tours as tour>
    <div class="col-lg-4">
        <section class="uui-image-text blue margin-top-36px">
            <div class="image-section"
                 style="background-image: url(<@spring.url '/'/>public/photo/${tour.photo});"></div>
            <div class="image-text-section">

                <ul class="col-md-8 parameters-tours">
                    <p class=""><@spring.message code="type"/>: <span class="font-weight-bold">${tour.tour}</span></p>
                    <p><@spring.message code="cost"/>: <span class="font-weight-bold">${tour.cost}</span></p>
                    <p><@spring.message code="duration"/>: <span class="font-weight-bold">${tour.duration}</span></p>
                    <p><@spring.message code="country"/>: <span class="font-weight-bold">${tour.country.name}</span></p>
                    <p><@spring.message code="date"/>: <span class="font-weight-bold">${tour.date}</span></p>
                </ul>
                <div class="col-md-4">
                    <div class="div-container-tour">
                        <@security.ifHasRole role="ADMIN">
                            <form  action="admin/tour/delet" method="POST">
                                <input type="hidden" value="${page}" name="page">
                                <button class="uui-button" name="tour_id" value="${tour.id}">Delete</button>
                            </form>
                        </@security.ifHasRole>
                        <a href="<@spring.url '/'/>tours/${tour.id}/"
                           class="uui-button lime-green right-icon margin-top-36px">
                            <@spring.message code="more_info"/><i class="fa fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </section>

    </div>
</#list>

<ul class="uui-pagination margin-top-72px margin-top-36px" >
    <li class="actions-wrapper">
        <ul class="pagination-items">
            <li class="first-page disable">
                <a href="<@spring.url '/'/>tours?page=0">
                    <span><@spring.message code="first"/></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pages-wrapper">
        <ul class="pagination-items">
            <#if (page > 0) >
                <li>
                    <a href="<@spring.url '/'/>tours?page=${page-1}">${page}</a>
                </li>
                <li class="active">
                    <a href="<@spring.url '/'/>tours/${page}">${page + 1}</a>
                </li>

                <#if page < (count/countPerPage) ?int>
                    <li>
                        <a href="<@spring.url '/'/>tours?page=${page + 1}">${page + 2}</a>
                    </li>
                </#if>
            <#else>

                <li class="active">
                    <a href="<@spring.url '/'/>tours?page=0">1</a>
                </li>

                <#if page < (count/countPerPage) ?int>
                    <li>
                        <a href="<@spring.url '/'/>tours?page=${page + 1}">2</a>
                    </li>
                </#if>


            </#if>


        </ul>
    </li>
    <li class="actions-wrapper">
        <ul class="pagination-items">
            <li class="last-page">
                <a href="<@spring.url '/'/>tours?page=${(count/countPerPage) ?int}">
                    <span><@spring.message code="last"/></span>
                </a>
            </li>
        </ul>
    </li>
</ul>
<#include "frames/footer.ftl">

