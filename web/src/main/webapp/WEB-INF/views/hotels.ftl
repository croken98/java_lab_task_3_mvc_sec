<#include "frames/header.ftl">
<h1 class="uui-promo-heading margin-top-36px margin-left-20px" ><@spring.message code="hotels"/></h1>

<@security.ifHasRole role="ADMIN">
    <div class="modal uui-modal fade" id="popup1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modal Tittle</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span></span><span></span>
                    </button>
                </div>
                <form method="POST" action="<@spring.url '/'/>admin/hotels/add">

                    <div class="modal-body">
                        <input type="hidden" name="page" value="${page}">
                        <input type="text" name="name" placeholder="name"><br>
                        <input type="text" name="website" placeholder="website"><br>
                        <input type="text" name="stars" placeholder="stars"><br>
                        <select name="features" multiple="multiple">
                            <#list features_array as feature>
                                <option value="${feature}">${feature.nameFeature}</option>
                            </#list>
                        </select>
                        <input type="text" name="lalitude" placeholder="lalitude"><br>
                        <input type="text" name="longitude" placeholder="longitude"><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="uui-button transparent">Cancel</button>
                        <button class="uui-button lime-green">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</@security.ifHasRole>



<@security.ifHasRole role="ADMIN">
    <div class="modal uui-modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modal Tittle</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <span></span><span></span>
                    </button>
                </div>
                <form method="POST" action="<@spring.url '/'/>admin/hotels/update">

                    <div class="modal-body">
                        <input type="hidden" name="page" value="${page}">
                        <input type="hidden" name="id" id="hotel_id_hidden">
                        <input type="text" name="name" placeholder="name"><br>
                        <input type="text" name="website" placeholder="website"><br>
                        <input type="text" name="stars" placeholder="stars"><br>
                        <select name="features" multiple="multiple">
                            <#list features_array as feature>
                                <option value="${feature}">${feature.nameFeature}</option>
                            </#list>
                        </select>
                        <input type="text" name="lalitude" placeholder="lalitude"><br>
                        <input type="text" name="longitude" placeholder="longitude"><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="uui-button transparent">Cancel</button>
                        <button class="uui-button lime-green">Save changes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</@security.ifHasRole>

<@security.ifHasRole role="ADMIN">
    <section class="admin-button-style">
        <button type="button" class="uui-button transparent" data-toggle="modal" data-target="#popup1">Add Hotel
        </button>
    </section>
</@security.ifHasRole>

<#list hotels as hotel>
    <div class="col-lg-4">
        <section class="uui-info-panel-vertical blue margin-bottom-36px" >
            <div class="info-panel-body">
                <div class="info-panel-section">

                    <@spring.message code="name"/>: <h2>${hotel.name}
                    </h2> <@spring.message code="stars"/>: <h2>${hotel.stars}
                    </h2><@spring.message code="website"/>: <h2>${hotel.website}
                    </h2> <@spring.message code="features"/>: <br>
                    <#list hotel.features as feature>
                        <h3>${feature.nameFeature}</h3>
                    </#list>
                </div>
            </div>
            <div class="info-panel-footer">
                <div class="info-panel-section">
                    <@security.ifHasRole role="ADMIN">
                        <form action="<@spring.url '/'/>admin/hotels/delete/" method="POST">

                            <input type="hidden" name="page" value="${page}">
                            <input type="hidden" name="hotel_id" value="${hotel.id}">
                            <button class="uui-button lime-green"><@spring.message code="delete"/></button>

                        </form>

                        <button type="button" class="uui-button transparent updateHotelModal" data-toggle="modal"
                                data-target="#updateModal" value="${hotel.id}"><@spring.message code="update"/>
                        </button>
                    </@security.ifHasRole>

                </div>
            </div>
        </section>
    </div>
</#list>

<ul class="uui-pagination margin-bottom-36px" >
    <li class="actions-wrapper">
        <ul class="pagination-items">
            <li class="first-page disable">
                <a href="<@spring.url '/'/>hotels?page=0">
                    <span><@spring.message code="first"/></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="pages-wrapper">
        <ul class="pagination-items">
            <#if (page > 0) >
                <li>
                    <a href="<@spring.url '/'/>hotels?page=${page-1}">${page}</a>
                </li>
                <li class="active">
                    <a href="<@spring.url '/'/>hotels?page=${page}">${page + 1}</a>
                </li>

                <#if page < (count/countPerPage) ?int>
                    <li>
                        <a href="<@spring.url '/'/>hotels?page=${page + 1}">${page + 2}</a>
                    </li>
                </#if>
            <#else>
                <li class="active">
                    <a href="<@spring.url '/'/>hotels?page=0">1</a>
                </li>
                <#if page < (count/countPerPage) ?int>
                    <li>
                        <a href="<@spring.url '/'/>hotels?page=${page + 1}">2</a>
                    </li>
                </#if>
            </#if>
        </ul>
    </li>
    <li class="actions-wrapper">
        <ul class="pagination-items">
            <li class="last-page">
                <a href="<@spring.url '/'/>hotels?page=${(count/countPerPage) ?int}">
                    <span><@spring.message code="last"/></span>
                </a>
            </li>
        </ul>
    </li>
</ul>
<script src="/public/js/script.js"></script>
<#include "frames/footer.ftl">