<#include "frames/header.ftl">
<link rel="stylesheet" href="public/uuicss/lib/components/datepicker3.css"/>
<script src="public/uui/js/lib/components/bootstrap-datepicker.js"></script>
<script src="public/uui/js/uui-datepicker.min.js"></script>
<link rel="stylesheet" href="public/uui/jquery-ui/jquery-ui.min.css"/>
<script src="public/uui/jquery-ui/jquery-ui.min.js"></script>

<link rel="stylesheet" href="public/uui/css/lib/components/datepicker3.css"/>
<script src="public/uui/js/lib/components/bootstrap-datepicker.js"></script>
<script src="public/uui/js/uui-datepicker.min.js"></script>

<@security.ifAuthenticated>
    <h1><@spring.message code="hi"/> ${user.login}!</h1><br>
</@security.ifAuthenticated>

<@security.ifNotAuthenticated>
    <h1><@spring.message code="anonymous"/></h1><br>
</@security.ifNotAuthenticated>

<h2> Find tours!</h2>

<form method="POST" action="<@spring.url '/'/>tours/search" class="col-md-3">
    <input type="hidden" id="minStars" name="minStars" value="0"><br><br>
    <input type="hidden" id="maxStars" name="maxStars" value="5"><br><br>
    <div class="stars slider-info">
        <@spring.message code="stars_range"/>:
        <span>1 - 5</span>
    </div>
    <div class="stars uui-slider range"></div>
    <input type="hidden" id="minCost" name="minCost" value="1"><br><br>
    <input type="hidden" id="maxCost" name="maxCost" value="100000"><br><br>
    <div class="cost slider-info">
        <@spring.message code="cost_range"/>:
        <span>$1 - $100000</span>
    </div>
    <div class="cost uui-slider range"></div>

    <input type="hidden" name="minDuration" id="minDuration" value="1"><br><br>
    <input type="hidden" name="maxDuration" id="maxDuration" value="365"><br><br>

    <div class="duration slider-info">
        <@spring.message code="duration_range"/>:
        <span>1 - 365</span>
    </div>
    <div class="duration uui-slider range"></div>
    <div class="uui-datepicker input-daterange input-group" id="datepicker2">
        <span class="input-group-addon"><@spring.message code="from"/></span>
        <input type="text" class="uui-form-element" name="minDate" placeholder="01/01/2017" value="01/01/2017"/>
        <span class="input-group-addon"><@spring.message code="to"/></span>
        <input type="text" class="uui-form-element" name="maxDate" placeholder="01/01/2020" value="01/01/2020"/>
    </div>
    <button class="uui-button blue margin-top-16px" ><@spring.message code="search"/></button>
</form>
<br><br>
<div class="col-lg-12">
    <#if tours ??>
        <#list tours as tour>
            <div class="col-lg-4">
                <section class="uui-image-text blue margin-top-36px">
                    <div class="image-section"
                         style="background-image: url(<@spring.url '/'/>public/photo/${tour.photo});"></div>
                    <div class="image-text-section">

                        <ul class="col-md-8 ndex-tours-container" >
                            <p class="">type: <span class="font-weight-bold">${tour.tour}</span></p>
                            <p><@spring.message code="cost"/>: <span class="font-weight-bold">${tour.cost}</span></p>
                            <p><@spring.message code="duration"/>: <span class="font-weight-bold">${tour.duration}</span></p>
                            <p><@spring.message code="country"/>: <span class="font-weight-bold">${tour.country.name}</span></p>
                            <p><@spring.message code="date"/>: <span class="font-weight-bold">${tour.date}</span></p>
                        </ul>
                        <div class="col-md-4">
                            <div class="index-main-container">
                                <@security.ifHasRole role="ADMIN">
                                    <form action="admin/tour/delete" method="POST">
                                        <input type="hidden" value="${page}" name="page">
                                        <button class="uui-button" name="tour_id" value="${tour.id}"><@spring.message code="delete"/></button>
                                    </form>
                                </@security.ifHasRole>
                                <a href="<@spring.url '/'/>tours/${tour.id}/"
                                   class="uui-button lime-green right-icon margin-top-36px">
                                    <@spring.message code="more_info"/><i class="fa fa-long-arrow-right"></i>
                                </a>

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </#list>
    </#if>
</div>
<script src="/public/js/script.js"></script>

<#include "frames/footer.ftl">

