var allButtons = document.getElementsByClassName("updateHotelModal");
for (var i = 0; i < allButtons.length; ++i) {
    allButtons[i].addEventListener("click", function (evt) {
        var id = evt.target.getAttribute("value");
        document.getElementById("hotel_id_hidden").setAttribute("value", id);
    })
}

$('.stars.uui-slider.range').each(
    function () {
        $(this).slider({
            range: true,
            min: 0,
            max: 5,
            values: [0, 5],
            slide: function (event, ui) {
                $(this).siblings('.stars.slider-info').find('span').text('' + ui.values[0] + ' - ' + ui.values[1]);
                $("#minStars").attr("value", ui.values[0])
                $("#maxStars").attr("value", ui.values[1])
            }
        });
    }
);


$('.cost.uui-slider.range').each(
    function () {
        $(this).slider({
            range: true,
            min: 1,
            max: 100000,
            values: [1, 100000],
            slide: function (event, ui) {
                $(this).siblings('.cost.slider-info').find('span').text('$' + ui.values[0] + ' - $' + ui.values[1]);
                $("#minCost").attr("value", ui.values[0])
                $("#maxCost").attr("value", ui.values[1])
            }
        });
    }
);


$('.duration.uui-slider.range').each(
    function () {
        $(this).slider({
            range: true,
            min: 1,
            max: 365,
            values: [1, 365],
            slide: function (event, ui) {
                $(this).siblings('.duration.slider-info').find('span').text('$' + ui.values[0] + ' - $' + ui.values[1]);
                $("#minDuration").attr("value", ui.values[0])
                $("#maxDuration").attr("value", ui.values[1])
            }
        });
    }
);

$('#datepicker2').uui_datepicker({todayHighlight: true});
$('.uui-rating').uui_rating();




ymaps.ready(init);

function init() {
    myMap = new ymaps.Map('map', {
        center: ("${tour.hotel.lalitude}" + "," + "${tour.hotel.longitude}").split(","),
        zoom: 10
    }, {
        searchControlProvider: 'yandex#search'
    });
    var location = ymaps.geolocation;
    location.get({
        provider: 'yandex',
        mapStateAutoApply: true
    }).then(function (result) {
        console.log(result);
        console.log(("${tour.hotel.lalitude}" + "," + "${tour.hotel.longitude}").split(","));
        result.geoObjects.position = ("${tour.hotel.lalitude}" + "," + "${tour.hotel.longitude}").split(",");
        console.log(result.geoObjects.position + "POSITION");
        result.geoObjects.options.set('preset', 'islands#redCircleIcon');
        result.geoObjects.get(0).properties.set({
            balloonContentBody: 'Мое местоположение'
        });
        myMap.geoObjects.add(result.geoObjects);
    });

    function createPlacemark(coords) {
        return new ymaps.Placemark(coords, {
            iconCaption: 'поиск...'
        }, {
            preset: 'islands#violetDotIconWithCaption',
            draggable: true
        });
    }

    getMyAdress(("${tour.hotel.lalitude}" + "," + "${tour.hotel.longitude}").split(","));

    function getMyAdress(coords) {
        var myPlacemark2;
        myPlacemark2 = createPlacemark(coords);
        myMap.geoObjects.add(myPlacemark2);
        myPlacemark2.events.add('dragend', function () {
            getAddress(myPlacemark2.geometry.getCoordinates());
        });
        myPlacemark2.properties.set('iconCaption', 'поиск...');
        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);
            myPlacemark2.properties
                .set({
                    iconCaption: [
                        firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                        firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                    ].filter(Boolean).join(', '),
                    balloonContent: firstGeoObject.getAddressLine()
                });
            document.getElementById("lastCordinates").innerText = myPlacemark2.properties._data.balloonContent;
        });
    }
}