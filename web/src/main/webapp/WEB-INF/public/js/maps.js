function initApp(str) {
    ymaps.ready(init);

    function init() {
        myMap = new ymaps.Map('map', {
            center: str,
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        });
        var location = ymaps.geolocation;
        location.get({
            provider: 'yandex',
            mapStateAutoApply: true
        }).then(function (result) {
            console.log(result);
            console.log(str);
            result.geoObjects.position = str;
            console.log(result.geoObjects.position + "POSITION");
            result.geoObjects.options.set('preset', 'islands#redCircleIcon');
            result.geoObjects.get(0).properties.set({
                balloonContentBody: 'Мое местоположение'
            });
            myMap.geoObjects.add(result.geoObjects);
        });

        function createPlacemark(coords) {
            return new ymaps.Placemark(coords, {
                iconCaption: 'поиск...'
            }, {
                preset: 'islands#violetDotIconWithCaption',
                draggable: true
            });
        }

        getMyAdress(str);

        function getMyAdress(coords) {
            var myPlacemark2;
            myPlacemark2 = createPlacemark(coords);
            myMap.geoObjects.add(myPlacemark2);
            myPlacemark2.events.add('dragend', function () {
                getAddress(myPlacemark2.geometry.getCoordinates());
            });
            myPlacemark2.properties.set('iconCaption', 'поиск...');
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                myPlacemark2.properties
                    .set({
                        iconCaption: [
                            firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                            firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                        ].filter(Boolean).join(', '),
                        balloonContent: firstGeoObject.getAddressLine()
                    });
                document.getElementById("lastCordinates").innerText = myPlacemark2.properties._data.balloonContent;
            });
        }
    }
}
