package by.zinkov.json;

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

public interface EntityParser<T> {
    List<T> parse(Reader reader) throws ParseException, java.text.ParseException, IOException;
}
