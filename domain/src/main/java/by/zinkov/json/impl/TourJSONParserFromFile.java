package by.zinkov.json.impl;

import by.zinkov.bean.Country;
import by.zinkov.bean.Hotel;
import by.zinkov.bean.Tour;
import by.zinkov.bean.TourType;
import by.zinkov.json.EntityParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Scope("prototype")
public class TourJSONParserFromFile implements EntityParser<Tour> {
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public List<Tour> parse(Reader reader) throws ParseException, java.text.ParseException, IOException {
        JSONParser parser = new JSONParser();
        JSONArray jsonArray = (JSONArray) parser.parse(reader);
        List<Tour> tours = new ArrayList<>();
        for (Object object : jsonArray) {
            JSONObject jsonObject = (JSONObject) object;
            Tour tour = new Tour();
            setPhoto(tour, jsonObject);
            setCost(tour, jsonObject);
            setCountry(tour, jsonObject);
            tour.setDescription((String) jsonObject.get("description"));
            tour.setDuration(((Long) jsonObject.get("duration")).intValue());
            setHotel(tour, jsonObject);
            tour.setTour(TourType.valueOf((String) jsonObject.get("tour")));
            tours.add(tour);
        }
        return tours;
    }

    private void setPhoto(Tour tour, JSONObject object) throws java.text.ParseException {
        tour.setPhoto((String) object.get("photo"));
        Date date = FORMATTER.parse((String) object.get("date"));
        tour.setDate(date);
    }

    private void setCost(Tour tour, JSONObject jsonObject) {
        BigDecimal cost = BigDecimal.valueOf((Double) jsonObject.get("cost"));
        tour.setCost(cost);
    }

    private void setCountry(Tour tour, JSONObject jsonObject) {
        Country country = new Country();
        country.setId(((Long) jsonObject.get("country_id")).intValue());
        tour.setCountry(country);
    }

    private void setHotel(Tour tour, JSONObject jsonObject) {
        Hotel hotel = new Hotel();
        hotel.setId(((Long) jsonObject.get("hotel_id")).intValue());
        tour.setHotel(hotel);
    }
}
