package by.zinkov.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LogginingAspect {
    @Pointcut("execution(* by.zinkov.repository.*.*(..))")
    private void allMethodsRepository() {
    }

    @Pointcut("execution(* by.zinkov.controller.*.*(..))")
    private void allMethodsController() {
    }

    @Pointcut("execution(* by.zinkov.service.*.*(..))")
    private void allMethodsService() {
    }

    @AfterThrowing(pointcut = "allMethodsRepository() || allMethodsService() || allMethodsController()", throwing = "e")
    public void logginingException(JoinPoint joinPoint, Exception e) throws Exception {
        Logger logger = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
        logger.error(e.toString());
    }
}
