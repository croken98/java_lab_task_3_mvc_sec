package by.zinkov.util;


import lombok.Data;

import java.util.HashMap;
import java.util.Map;

public class SearchCriteria {
    private Map<String, Object> mapCriteriaValuesRange = new HashMap<>();

    public void setMax(String criteria, Object maxValue) {
        Pair range = (Pair) mapCriteriaValuesRange.get(criteria);
        if (range == null) {
            range = new Pair();
            mapCriteriaValuesRange.put(criteria, range);
        }
        range.max = maxValue;
    }

    public void setMin(String criteria, Object minValue) {
        Pair range = (Pair) mapCriteriaValuesRange.get(criteria);
        if (range == null) {
            range = new Pair();
            mapCriteriaValuesRange.put(criteria, range);
        }
        range.min = minValue;
    }

    public <T> T getMin(String criteria, Class<T> objectClass) {
        Pair range = (Pair) mapCriteriaValuesRange.get(criteria);
        if (range == null) {
            return null;
        }
        return (T) range.min;
    }

    public <T> T getMax(String criteria, Class<T> objectClass) {
        Pair range = (Pair) mapCriteriaValuesRange.get(criteria);
        if (range == null) {
            return null;
        }
        return (T) range.max;
    }

    public void setConcreteValue(String criteria, Object value) {
        mapCriteriaValuesRange.put(criteria, value);
    }

    public <T> T getConcreteValue(String criteria, Class<T> objectClass) {
        return (T) mapCriteriaValuesRange.get(criteria);
    }


    @Data
    private final class Pair<T> {
        T max;
        T min;
    }
}
