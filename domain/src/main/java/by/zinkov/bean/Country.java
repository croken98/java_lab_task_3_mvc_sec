package by.zinkov.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "country")
public class Country implements Identity<Integer>{
    private static final int MAX_NAME_LENGTH = 40;

    @Override
    public Integer getId(){
        return id;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    @NotNull
    @Size(max = MAX_NAME_LENGTH)
    private String name;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany( mappedBy = "country" , fetch = FetchType.LAZY)
    private List<Tour> tours = new ArrayList<>();
}
