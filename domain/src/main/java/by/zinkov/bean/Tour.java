package by.zinkov.bean;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "tour")
@TypeDef(
        name = "tour_type_enum",
        typeClass = PostgreSQLEnumType.class
)
public class Tour implements Identity<Integer>{
    private static final int MAX_DURATION_LENGTH = 365;
    private static final int MAX_DESCRIPTION_LENGTH = 1000;
    private static final int MAX_DECIMAL_SIZE = 100000;
    private static final int MAX_PHOTO_LENGTH = 400;



    @Override
    public Integer getId(){
        return id;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(max = MAX_PHOTO_LENGTH)
    private String photo;

    @Column
    private Date date;

    @Column
    @NotNull
    @Max(MAX_DURATION_LENGTH)
    private long duration;

    @Column
    @NotNull
    @Size(max = MAX_DESCRIPTION_LENGTH)
    private String description;

    @Column
    @NotNull
    @Max(MAX_DECIMAL_SIZE)
    private BigDecimal cost;
    @Column
    @Enumerated(EnumType.STRING)
    @Type(type = "tour_type_enum")
    private TourType tour;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id")
    private Country country;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "tours", fetch = FetchType.LAZY)
    private List<User> users = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "tour", fetch = FetchType.LAZY)
    private List<Review> reviews = new ArrayList<>();


}
