package by.zinkov.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "review")
public class Review implements Identity<Integer>, Serializable {
    private static final int MAX_TEXT_LENGTH = 1000;

    @Override
    public Integer getId(){
        return id;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    @NotNull
    @NotEmpty
    @Size(min = 1,max = MAX_TEXT_LENGTH)
    private String text;

    @Column
    private Timestamp date;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name = "tour_id")
    private Tour tour;

}
