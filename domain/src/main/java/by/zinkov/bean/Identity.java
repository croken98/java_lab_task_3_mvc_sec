package by.zinkov.bean;

public interface Identity<PK> {
    PK getId();
}
