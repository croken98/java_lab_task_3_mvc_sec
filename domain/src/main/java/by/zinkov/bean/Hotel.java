package by.zinkov.bean;

import com.vladmihalcea.hibernate.type.array.EnumArrayType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "hotel")

@TypeDef(
        typeClass = EnumArrayType.class,
        defaultForType = Features[].class,
        parameters = {
                @Parameter(
                        name = EnumArrayType.SQL_ARRAY_TYPE,
                        value = "features_enum"
                )
        }
)

public class Hotel implements Identity<Integer>{
    private static final int MAX_NAME_LENGTH = 45;
    private static final int MAX_STARS_LENGTH = 5;
    private static final int MAX_WEBSITE_LENGTH = 100;
    private static final int MAX_COORDINATES_LENGTH = 100;

    @Override
    public Integer getId(){
        return id;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    @NotNull
    @Size(max = MAX_NAME_LENGTH)
    private String name;

    @Column
    @NotNull
    @Max(MAX_STARS_LENGTH)
    private byte stars;

    @Column
    @NotNull
    @Size(max = MAX_WEBSITE_LENGTH)
    private String website;

    @Column
    @NotNull
    @Size(max = MAX_COORDINATES_LENGTH)
    private String lalitude;

    @Column
    @NotNull
    @Size(max = MAX_COORDINATES_LENGTH)
    private String longitude;


    @Column(
            name = "features",
            columnDefinition = "features_enum[]"
    )
    private Features[] features;

    @ToString.Exclude
    @OneToMany(mappedBy = "hotel", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private List<Tour> tours = new ArrayList<>();


}
