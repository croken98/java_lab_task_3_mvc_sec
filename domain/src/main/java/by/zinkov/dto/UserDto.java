package by.zinkov.dto;

import by.zinkov.bean.Role;
import by.zinkov.validation.PasswordMatches;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@PasswordMatches
@Data
public class UserDto {
    private static final int MAX_LOGIN_LENGTH = 45;
    private static final int PASSWORD_LENGTH = 64;

    private int id;
    @Size(max = MAX_LOGIN_LENGTH)
    @NotNull
    @NotEmpty
    private String login;

    @Size(max = PASSWORD_LENGTH)
    @NotNull
    @NotEmpty
    private String password;

    private String matchingPassword;

    private Role role;
}
