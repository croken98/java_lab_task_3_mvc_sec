package by.zinkov.impl;

import by.zinkov.bean.*;
import by.zinkov.config.DBConfigForTests;
import by.zinkov.repository.impl.CountryRepository;
import by.zinkov.repository.impl.HotelRepository;
import by.zinkov.repository.impl.TourRepository;
import by.zinkov.specification.GetAllSpecification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Transactional("transactionManager")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class})
public class TourRepositoryTest extends AbstractTest {
    @Autowired
    TourRepository tourRepository;

    @Autowired
    GetAllSpecification<Tour> allToursSpecification;
    @Autowired
    HotelRepository hotelRepository;

    @Autowired
    CountryRepository countryRepository;

/*    @Test
    public void shouldReturnTwoToursFromDB() {
        Tour tourOne = new Tour();
        Tour tourTwo = new Tour();
        setDefaultTour(tourOne);
        setDefaultTour(tourTwo);
        tourRepository.save(tourOne);
        tourRepository.save(tourTwo);
        List<Tour> tours = tourRepository.query(allToursSpecification);
        Assert.assertEquals(2, tours.size());
        Assert.assertEquals(tourOne.getDescription(), tours.get(0).getDescription());
        Assert.assertEquals(tourTwo.getDescription(), tours.get(1).getDescription());
    }*/

   /* @Test
    public void shouldAddTour() {
        Tour tour = new Tour();
        setDefaultTour(tour);
        Optional<Tour> tourOptional = tourRepository.save(tour);
        Tour expectedTour = tourRepository.getByPK(1).get();
        setDefaultTour(expectedTour);
        Assert.assertEquals(expectedTour.getDescription(), tourOptional.get().getDescription());
        Assert.assertEquals(expectedTour.getDescription(), tourOptional.get().getDescription());
        Assert.assertEquals(expectedTour.getId(), tourOptional.get().getId());
    }*/

  /*  @Test
    public void shouldAddAndRemoveObj() {
        Tour tour = new Tour();
        setDefaultTour(tour);
        Optional<Tour> tourOptional = tourRepository.save(tour);
        tourRepository.delete(tourOptional.get());
        tourOptional = tourRepository.getByPK(tourOptional.get().getId());
        Assert.assertFalse(tourOptional.isPresent());
    }*/

    @Test
    public void shouldGetByPrimaryKeyNotExistingEntityAndReturnEmptyOptional() {
        Optional<Tour> tour = tourRepository.getByPK(1);
        Assert.assertFalse(tour.isPresent());
    }

   /* @Test
    public void shouldInsertTourAndUpdateTour() {
        Tour tour = new Tour();
        setDefaultTour(tour);
        Optional<Tour> tourOptional = tourRepository.save(tour);
        Tour updatedTour = tourOptional.get();
        updatedTour.setDescription("new descr");
        tourRepository.update(updatedTour);
        tourOptional = tourRepository.getByPK(updatedTour.getId());
        Assert.assertEquals("new descr", tourOptional.get().getDescription());
    }*/


    @Before
    public void initDb() {
        Country country = new Country();
        country.setName("RB");
        countryRepository.save(country);
        Hotel hotel = new Hotel();
        hotel.setName("Hotel");
        hotel.setStars((byte) 3);
        hotel.setWebsite("http//:hotel.com");
        hotel.setLalitude("12,23451");
        hotel.setLongitude("234,23231");
        hotel.setFeatures(new Features[]{Features.INDOOR_HOTEL_POOL, Features.BILLIARD_TABLE});
        hotelRepository.save(hotel);
    }


 /*   private Tour setDefaultTour(Tour tour) {
        tour.setCost(new BigDecimal(100));
        tour.setCountry(countryRepository.getByPK(1).get());
        tour.setDate(new Timestamp(23432342564L));
        tour.setDescription("description");
        tour.setDuration(7);
        Hotel hotel = hotelRepository.getByPK(1).get();
        tour.setHotel(hotel);
        tour.setPhoto(new byte[]{1, 3, 5, 123, 23, 100});
        tour.setTour(TourType.THE_GROUP_TOUR);
        return tour;
    }*/
}