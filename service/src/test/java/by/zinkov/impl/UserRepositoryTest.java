package by.zinkov.impl;

import by.zinkov.bean.Role;
import by.zinkov.bean.User;
import by.zinkov.config.DBConfigForTests;
import by.zinkov.repository.impl.UserRepository;
import by.zinkov.specification.GetAllSpecification;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional("transactionManager")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class})
public class UserRepositoryTest extends AbstractTest {

    @Autowired
    UserRepository userRepository;
   // @Autowired
    //RoleTableRepository roleTableRepository;

    @Autowired
    private GetAllSpecification<User> allUsersSpecification;

  /*  @Test
    public void shouldAddUser() {
        User user = getDefaultUser();
        RoleTable roleTable = roleTableRepository.getByPK(Role.ADMIN).get();
        user.setRole(roleTable);
        Optional<User> userOptional = userRepository.save(user);
        user.setRole(roleTable);
        User expectedUser = getDefaultUser();
        expectedUser.setId(1);
        expectedUser.setRole(roleTable);
        Assert.assertEquals(expectedUser, userOptional.get());

    }
*/
    @Test
    public void shouldReturnTwoUsersFromDB() {
        User userOne = getDefaultUser();
        User userTwo = getDefaultUser();
        userRepository.save(userOne);
        userRepository.save(userTwo);
        List<User> users = userRepository.query(allUsersSpecification);
        Assert.assertEquals(2, users.size());
        Assert.assertEquals(userOne.getLogin(), users.get(0).getLogin());
        Assert.assertEquals(userTwo.getLogin(), users.get(1).getLogin());
    }

    @Test
    public void shouldAddAndRemoveObj() {
        User user = getDefaultUser();
        Optional<User> userOptional = userRepository.save(user);
        userRepository.delete(userOptional.get());

        userOptional = userRepository.getByPK(userOptional.get().getId());
        Assert.assertFalse(userOptional.isPresent());
    }

    @Test
    public void shouldGetByPrimaryKeyNotExistingEntityAndReturnEmptyOptional() {
        Optional<User> user = userRepository.getByPK(1);
        Assert.assertFalse(user.isPresent());
    }

    @Test
    public void shouldInsertUserAndUpdateUser() {
        User user = getDefaultUser();
        Optional<User> userOptional = userRepository.save(user);
        User updatedUser = userOptional.get();
        updatedUser.setLogin("new login");
        userRepository.update(updatedUser);
        System.out.println(updatedUser + "\n\n\n\n\n\n\n\n\n\n\n\n\n");
        userOptional = userRepository.getByPK(updatedUser.getId());
        Assert.assertEquals("new login", userOptional.get().getLogin());
    }

    @Test
    public void shouldGetNotExistingUserAndReturnEmptyOptional() {
        Optional<User> userOptional = userRepository.getByPK(10000);
        Assert.assertEquals(Optional.empty(), userOptional);
    }

    private User getDefaultUser() {
        User user = new User();
        user.setLogin("login");
        user.setPassword("passpasspasspasspasspasspasspass");
        return user;
    }
}