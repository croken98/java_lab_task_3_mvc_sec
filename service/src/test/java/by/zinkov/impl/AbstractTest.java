package by.zinkov.impl;

import by.zinkov.config.DBConfigForTests;
import org.flywaydb.core.Flyway;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class})
public class AbstractTest {
    @Autowired
    private Flyway flyway;

    @Transactional
    @Before
    public void migrate() {
        flyway.clean();
        flyway.migrate();
    }

    @Transactional
    @Test
    public void defaultTest() {
        Assert.assertTrue(true);
    }
}
