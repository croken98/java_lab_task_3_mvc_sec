package by.zinkov.impl;

import by.zinkov.bean.*;
import by.zinkov.config.DBConfigForTests;
import by.zinkov.repository.impl.*;
import by.zinkov.specification.GetAllSpecification;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class})
public class ReviewRepositoryTest extends AbstractTest {

    private static final String TEST_TEXT = "TEST_TEXT";
    private static final String TEST_LOGIN = "login";
    private static final String TEST_PASSWORD = "passpasspasspasspasspasspasspass";
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private TourRepository tourRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private GetAllSpecification<Review> allReviewsSpecification;

    @Transactional("transactionManager")
    @Test
    public void shouldAddReview() {
        Review review = getDefaultReview();
        Optional<Review> reviewOptional = reviewRepository.save(review);
        Review expectedReview = reviewRepository.getByPK(1).get();
        Assert.assertEquals(expectedReview.getText(), reviewOptional.get().getText());
        Assert.assertEquals(1, reviewRepository.query(allReviewsSpecification).size());
    }

    @Transactional("transactionManager")
    @Test
    public void shouldAddAndRemoveObj() {
        Review review = getDefaultReview();
        Optional<Review> reviewOptional = reviewRepository.save(review);
        reviewRepository.delete(reviewOptional.get());
        reviewOptional = reviewRepository.getByPK(reviewOptional.get().getId());
        Assert.assertFalse(reviewOptional.isPresent());
    }

    @Transactional("transactionManager")
    @Test
    public void shouldReturnTwoReviewsFromDB() {
        Review reviewlOne = getDefaultReview();
        Review reviewTwo = getDefaultReview();
        reviewRepository.save(reviewlOne);
        reviewRepository.save(reviewTwo);
        List<Review> reviews = reviewRepository.query(allReviewsSpecification);
        Assert.assertEquals(2, reviews.size());
        Assert.assertEquals(reviewlOne.getText(), reviews.get(0).getText());
        Assert.assertEquals(reviewTwo.getText(), reviews.get(1).getText());
    }

    @Transactional("transactionManager")
    @Test
    public void shouldGetByPrimaryKeyNotExistingEntityAndReturnEmptyOptional() {
        Optional<Review> review = reviewRepository.getByPK(1);
        Assert.assertFalse(review.isPresent());
    }

    @Transactional("transactionManager")
    @Test
    public void shouldInsertUserAndUpdateUser() {
        Review review = getDefaultReview();
        Optional<Review> reviewOptional = reviewRepository.save(review);
        Review updatedReview = reviewOptional.get();
        updatedReview.setText(TEST_TEXT);
        reviewRepository.update(updatedReview);
        reviewOptional = reviewRepository.getByPK(updatedReview.getId());
        Assert.assertEquals(TEST_TEXT, reviewOptional.get().getText());
    }

   /* @Before
    public void initDB() {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setPassword("passpasspasspasspasspasspasspass");
        userRepository.save(user);

        Country country = new Country();
        country.setName("RB");
        countryRepository.save(country);

        Hotel hotel = new Hotel();
        hotel.setName("Hotel");
        hotel.setStars((byte) 3);
        hotel.setWebsite("http//:hotel.com");
        hotel.setLalitude("12,23451");
        hotel.setLongitude("234,23231");
        hotel.setFeatures(new Features[]{Features.INDOOR_HOTEL_POOL, Features.BILLIARD_TABLE});
        hotelRepository.save(hotel);

        Tour tour = new Tour();
        tour.setCost(new BigDecimal(100));
        tour.setCountry(countryRepository.getByPK(1).get());
        tour.setDate(new Timestamp(23432342564L));
        tour.setDescription("description");
        tour.setDuration(7);
        tour.setHotel(hotel);
        tour.setPhoto(new byte[]{1, 3, 5, 123, 23, 100});
        tour.setTour(TourType.THE_GROUP_TOUR);
        tourRepository.save(tour);
        transaction.commit();
    }*/

    private Review getDefaultReview() {
        Review review = new Review();
        review.setText("tour text");
        review.setTour(tourRepository.getByPK(1).get());
        review.setDate(new Timestamp(12423553));
        review.setUser(userRepository.getByPK(1).get());
        return review;
    }
}