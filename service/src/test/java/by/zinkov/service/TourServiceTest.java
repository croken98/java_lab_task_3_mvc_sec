package by.zinkov.service;

import by.zinkov.bean.Tour;
import by.zinkov.repository.RepositoryFactory;
import by.zinkov.repository.impl.TourRepository;
import by.zinkov.service.impl.TourService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TourServiceTest {

    @Mock
    private RepositoryFactory repositoryFactoryMock;

    @Mock
    private TourRepository tourRepositoryMock;

    @InjectMocks
    private TourService tourService;


    @Before
    public void usualMocks() {
        when(repositoryFactoryMock.getRepository(TourRepository.class)).thenReturn(tourRepositoryMock);
    }

    @Test
    public void whenCallInsertReturnOptionalWithId() {

        Tour tourOne = new Tour();
        tourOne.setDescription("BY");

        Tour tourTwo = new Tour();
        tourTwo.setDescription("BY");
        tourTwo.setId(1);
        when(tourRepositoryMock.save(tourOne)).thenReturn(Optional.of(tourTwo));
        Optional<Tour> optionalTour = tourService.add(tourOne);
        Mockito.verify(tourRepositoryMock, Mockito.times(1)).save(tourOne);

        Assert.assertEquals(optionalTour, Optional.of(tourTwo));

    }

    @Test
    public void removeTest() {
        Tour tour = new Tour();
        tour.setDescription("BY");
        tour.setId(1);
        tourService.remove(tour);
        Mockito.verify(tourRepositoryMock, Mockito.times(1)).delete(tour);
    }

    @Test
    public void updateTest() {
        Tour tour = new Tour();
        tour.setDescription("BY");
        tour.setId(1);
        tourService.update(tour);
        Mockito.verify(tourRepositoryMock, Mockito.times(1)).update(tour);
    }

    @Test
    public void whenCallGetByPKReturnOptional() {
        Tour tour = new Tour();
        tour.setDescription("BY");
        tour.setId(1);
        when(tourRepositoryMock.getByPK(tour.getId())).thenReturn(Optional.of(tour));
        Optional<Tour> optionalTour = tourService.getByPk(tour.getId());
        Mockito.verify(tourRepositoryMock, Mockito.times(1)).getByPK(tour.getId());
        Assert.assertEquals(tour, optionalTour.get());
    }
}