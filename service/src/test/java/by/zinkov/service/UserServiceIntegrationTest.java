package by.zinkov.service;

import by.zinkov.bean.*;
import by.zinkov.config.DBConfigForTests;
import by.zinkov.impl.AbstractTest;
import by.zinkov.service.impl.*;
import by.zinkov.util.SearchCriteria;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DBConfigForTests.class})
public class UserServiceIntegrationTest extends AbstractTest {
    private static final String COUNTRY_ENTITY_FIELD = "country";
    private static final String STARS_ENTITY_FIELD = "stars";
    private static final String COST_ENTITY_FIELD = "cost";
    private static final String DURATION_ENTITY_FIELD = "duration";
    private static final String DATE_ENTITY_FIELD = "date";

    private static final String COUNTRY_RB_TEST = "RB";

    private static final Logger logger = LoggerFactory.getLogger(UserServiceIntegrationTest.class);
    private static final String TEST_LOGIN = "login";

    @Autowired
    private UserService userService;
    @Autowired
    private CountryService countryService;
    @Autowired
    private HotelService hotelService;
    @Autowired
    private TourService tourService;
    @Autowired
    private ReviewService reviewService;

   /* @Test
    public void getUserReviewsTest() {
        Assert.assertTrue(userService.getByPk(1).isPresent());

        User user = userService.getByPk(1).get();

        List<Review> reviews = userService.getUserReviews(user);
        Assert.assertTrue(reviews != null);

        Assert.assertEquals(2, reviewService.getAll().size());
        Assert.assertEquals(2, reviews.size());
    }*/

    @Test
    public void getAllReviewsForTourTest() {
        Tour tour = tourService.getByPk(1).get();
        List<Review> reviews = tourService.getTourReviews(tour);
        Assert.assertTrue(reviews != null);
        Assert.assertEquals(2, reviews.size());
    }

    /*@Test
    public void getUserToursTest() {
        Assert.assertTrue(userService.getByPk(1).isPresent());
        User user = userService.getByPk(1).get();
        List<Tour> tours = userService.getUserTours(user);
        Assert.assertTrue(tours != null);

        Assert.assertEquals(2, tours.size());
    }*/

    @Test
    public void getToursBySearchCriteriaTest() {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setConcreteValue(COUNTRY_ENTITY_FIELD, countryService.getAll());
        searchCriteria.setMin(STARS_ENTITY_FIELD, (byte) 1);
        searchCriteria.setMax(STARS_ENTITY_FIELD, (byte) 5);

        searchCriteria.setMin(COST_ENTITY_FIELD, new BigDecimal(10));
        searchCriteria.setMax(COST_ENTITY_FIELD, new BigDecimal(1100));

        searchCriteria.setMin(DURATION_ENTITY_FIELD, 3L);
        searchCriteria.setMax(DURATION_ENTITY_FIELD, 10L);

        searchCriteria.setMin(DATE_ENTITY_FIELD, new Date(2343234L));
        searchCriteria.setMax(DATE_ENTITY_FIELD, new Date(234323242564L));

        List<Tour> tours;
        searchCriteria.setConcreteValue(COUNTRY_ENTITY_FIELD, Arrays.asList(countryService.getByPk(1).get()));
        tours = tourService.getToursBySearchCriteria(searchCriteria);
        Assert.assertEquals(1, tours.size());
        Assert.assertEquals("RB", tours.get(0).getCountry().getName());

    }

  /*  @Transactional
    @Before
    public void initDB() {
        User user = new User();
        user.setLogin(TEST_LOGIN);
        user.setPassword("passpasspasspasspasspasspasspass");
        user = userService.add(user).get();

        Country country = new Country();
        country.setName(COUNTRY_RB_TEST);
        countryService.add(country);

        Country country2 = new Country();
        country2.setName("RU");
        countryService.add(country2);

        Hotel hotel = new Hotel();
        hotel.setName("Hotel");
        hotel.setStars((byte) 3);
        hotel.setWebsite("http//:hotel.com");
        hotel.setLalitude("12,23451");
        hotel.setLongitude("234,23231");
        hotel.setFeatures(new Features[]{Features.INDOOR_HOTEL_POOL, Features.BILLIARD_TABLE});
        hotelService.add(hotel);

        Tour tour = new Tour();
        tour.setCost(new BigDecimal(100));
        tour.setCountry(countryService.getByPk(1).get());
        tour.setDate(new Date(23432342564L));
        tour.setDescription("description");
        tour.setDuration(7);
        tour.setHotel(hotel);
        tour.setPhoto(new byte[]{1, 3, 5, 123, 23, 100});
        tour.setTour(TourType.THE_GROUP_TOUR);
        tourService.add(tour);

        Tour secondTour = new Tour();
        secondTour.setCost(new BigDecimal(1000));
        secondTour.setCountry(countryService.getByPk(2).get());
        secondTour.setDate(new Date(23432342564L));
        secondTour.setDescription("description2");
        secondTour.setDuration(7);
        secondTour.setHotel(hotel);
        secondTour.setPhoto(new byte[]{1, 3, 5, 123, 25, 100});
        secondTour.setTour(TourType.BUSINESS_TRAVEL);
        tourService.add(secondTour);
        userService.addTour(user, tour);
        userService.addTour(user, secondTour);

        Review review = new Review();
        review.setText("Bad");
        review.setDate(new Timestamp(12423553));
        review.setTour(tour);
        review.setUser(user);
        reviewService.add(review);


        Review secondreview = new Review();
        secondreview.setText("Bad 2");
        secondreview.setDate(new Timestamp(12423553));
        secondreview.setTour(tour);
        secondreview.setUser(user);
        Optional<Review> review1 = reviewService.add(secondreview);
    }*/
}
