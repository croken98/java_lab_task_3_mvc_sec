package by.zinkov.config;

import by.zinkov.convertors.EntityDtoConvertor;
import by.zinkov.repository.Repository;
import by.zinkov.repository.impl.CountryRepository;
import by.zinkov.service.Service;
import by.zinkov.service.impl.CountryService;
import by.zinkov.specification.impl.AllCountriesSpecification;
import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@EnableAspectJAutoProxy
@EnableTransactionManagement
@Configuration
@ComponentScan(basePackageClasses = {Repository.class , Service.class , AllCountriesSpecification.class , CountryService.class, CountryRepository.class, EntityDtoConvertor.class})
public class DBConfigForTests {

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(entityManagerFactory());
        return manager;
    }

    @Autowired
    EmbeddedPostgres testDatabase;

    @Bean()
    public DataSource dataSource() {
        return testDatabase.getPostgresDatabase();
    }

    @Bean
    public EmbeddedPostgres embeddedPostgres() throws IOException {
        return EmbeddedPostgres.start();
    }

    @Bean
    public Flyway flyway() {
        return Flyway.configure().dataSource(testDatabase.getPostgresDatabase()).envVars().load();
    }

    @Bean
    public EntityManager entityManager() {
        return entityManagerFactory().createEntityManager();
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        Map<String, Object> addedOrOverridenProperties = new HashMap<>();
        addedOrOverridenProperties.put("hibernate.connection.datasource", dataSource());
        return Persistence.createEntityManagerFactory("persistence_unit_one", addedOrOverridenProperties);
    }

    @Bean
    public ValidatorFactory validatorFactory(){
        return Validation.buildDefaultValidatorFactory();
    }
    @Bean
    public Validator validator(){
        return validatorFactory().getValidator();
    }
}