package by.zinkov.service;

import by.zinkov.bean.Identity;
import by.zinkov.repository.AbstractRepository;
import by.zinkov.specification.GetAllSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Component
public abstract class Service<T extends Identity<PK>, PK> {

    private static final int BATCH_SIZE = 100;
    @Autowired
    protected GetAllSpecification<T> getAllSpecification;
    @Autowired
    protected AbstractRepository<T , PK> repository;

    @Transactional("transactionManager")
    public void insertBath(List<T> objects) {
        repository.insertBatch(objects, BATCH_SIZE);
    }

    @Transactional("transactionManager")
    public Optional<T> add(T object) {
        return repository.save(object);
    }

    @Transactional("transactionManager")
    public void remove(T object) {
        repository.delete(object);
    }

    @Transactional("transactionManager")
    public void update(T object) {
        repository.update(object);
    }

    @Transactional("transactionManager")
    public Optional<T> getByPk(PK id) {
        return repository.getByPK(id);
    }

    public List<T> getAll() {
        return repository.query(getAllSpecification);
    }

    @Transactional
    public long getCount() {
        return repository.getCount();
    }
}
