package by.zinkov.service.impl;

import by.zinkov.bean.Review;
import by.zinkov.bean.Tour;
import by.zinkov.specification.PaginationSpecification;
import by.zinkov.specification.impl.GetBySearchCriteriaSpecification;
import by.zinkov.specification.impl.PaginationToursSpecification;
import by.zinkov.util.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class TourService extends by.zinkov.service.Service<Tour, Integer> {
    @Autowired
    private ApplicationContext context;

    @Transactional
    public List<Review> getTourReviews(Tour tour) {
        tour = repository.getByPK(tour.getId()).get();
        tour.getReviews().size();
        return tour.getReviews();
    }

    @Transactional
    public List<Tour> getToursBySearchCriteria(SearchCriteria criteria) {
        GetBySearchCriteriaSpecification specification = context.getBean(GetBySearchCriteriaSpecification.class);
        specification.setSearchCriteria(criteria);
        return repository.query(specification);
    }

    @Transactional
    public List<Tour> getPaginatedTours(int pageNumber, int countPerPage) {
        PaginationSpecification<Tour> specification = context.getBean(PaginationToursSpecification.class);
        specification.setPage(pageNumber);
        specification.setCount(countPerPage);
        return repository.query(specification);
    }
}
