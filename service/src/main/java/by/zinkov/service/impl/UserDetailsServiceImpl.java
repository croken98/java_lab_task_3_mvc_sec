package by.zinkov.service.impl;

import by.zinkov.bean.User;
import by.zinkov.repository.impl.UserRepository;
import by.zinkov.specification.impl.GetUserByLoginSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    ApplicationContext context;

    public UserDetails loadUserByUsername(String login)
            throws UsernameNotFoundException {
        GetUserByLoginSpecification specification= context.getBean(GetUserByLoginSpecification.class);
        specification.setLogin(login);
        User user = userRepository.query(specification).get(0);
        if (user == null) {
            throw new UsernameNotFoundException(
                    "No user found with login: "+ login);
        }
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
//        return  new org.springframework.security.core.userdetails.User
//                (user.getLogin(),
//                        user.getPassword().toLowerCase(), enabled, accountNonExpired,
//                        credentialsNonExpired, accountNonLocked,
//                        getAuthorities(user.getLogin()));
        return null;
    }

    private static List<GrantedAuthority> getAuthorities (List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }
}