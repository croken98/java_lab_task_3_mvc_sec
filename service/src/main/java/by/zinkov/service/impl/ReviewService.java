package by.zinkov.service.impl;

import by.zinkov.bean.Review;
import by.zinkov.service.Service;
import org.springframework.stereotype.Component;

@Component
public class ReviewService extends Service<Review,Integer> {
}
