package by.zinkov.service.impl;

import by.zinkov.bean.Review;
import by.zinkov.bean.Role;
import by.zinkov.bean.Tour;
import by.zinkov.bean.User;
import by.zinkov.convertors.EntityDtoConvertor;
import by.zinkov.dto.UserDto;
import by.zinkov.exception.LoginAlreadyExistException;
import by.zinkov.specification.impl.GetUserByLoginSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class UserService extends by.zinkov.service.Service<User, Integer> {
    @Autowired
    private EntityDtoConvertor<User, UserDto> userDtoEntityConverter;
    @Autowired
    private ApplicationContext context;
    @Autowired
    private PasswordEncoder encoder;


    @Transactional("transactionManager")
    public List<Review> getUserReviews(UserDto userDto) {
        User user = repository.getByPK(userDto.getId()).orElseThrow(RuntimeException::new);
        user.getReviews().size();
        return user.getReviews();
    }

    @Transactional("transactionManager")
    public List<Tour> getUserTours(UserDto userDto) {
        User user = repository.getByPK(userDto.getId()).orElseThrow(RuntimeException::new);
        user.getTours().size();
        return user.getTours();
    }

    @Transactional("transactionManager")
    public void addTour(UserDto userDto, Tour tour) {
        User user = repository.getByPK(userDto.getId()).orElseThrow(RuntimeException::new);
        user.getTours().add(tour);
        repository.update(user);
    }

    private boolean haveThisLogin(String login) {
        GetUserByLoginSpecification specification = context.getBean(GetUserByLoginSpecification.class);
        specification.setLogin(login);
        List<User> user = repository.query(specification);
        return !user.isEmpty();
    }

    public UserDto getByLogin(String login) {
        GetUserByLoginSpecification specification = context.getBean(GetUserByLoginSpecification.class);
        specification.setLogin(login);
        List<User> user = repository.query(specification);
        UserDto userDto = !user.isEmpty() ? userDtoEntityConverter.toDTO(user.get(0)) : null;
        return userDto;
    }

    @Transactional("transactionManager")
    public UserDto registerNewUserAccount(UserDto userDto) throws LoginAlreadyExistException {
        if (this.haveThisLogin(userDto.getLogin())) {
            throw new LoginAlreadyExistException(userDto.getLogin());
        } else {
            User user = userDtoEntityConverter.toEntity(userDto);
            user.setRole(Role.USER);
            user = repository.save(user).orElseThrow(RuntimeException::new);
            return userDtoEntityConverter.toDTO(user);
        }
    }

    @Transactional
    public boolean hasThisTour(UserDto userDto, int tourId) {
        List<Tour> tours = getUserTours(userDto);
        return tours.stream()
                .mapToInt((Tour::getId))
                .anyMatch(id -> tourId == id);
    }

    @Transactional
    public void removeTourFromUser(UserDto userDto, int tourId){
        User user = repository.getByPK(userDto.getId()).orElseThrow(RuntimeException::new);
        List<Tour> tours=  user.getTours();
        Tour tour = tours.stream().filter(t->t.getId()==tourId).findFirst().get();
        tours.remove(tour);
        repository.update(user);
    }
}
