package by.zinkov.service.impl;

import by.zinkov.bean.Hotel;
import by.zinkov.specification.PaginationSpecification;
import by.zinkov.specification.impl.PaginationHotelsSpecifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HotelService extends by.zinkov.service.Service<Hotel,Integer> {
    @Autowired
    private ApplicationContext context;

    public List<Hotel> getPaginatedHotels(int pageNumber,int countPerPage){
        PaginationSpecification<Hotel> specification = context.getBean(PaginationHotelsSpecifications.class);
        specification.setCount(countPerPage);
        specification.setPage(pageNumber);
        return repository.query(specification);
    }
}
