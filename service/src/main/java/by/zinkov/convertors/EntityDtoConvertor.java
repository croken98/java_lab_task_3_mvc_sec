package by.zinkov.convertors;

public interface EntityDtoConvertor<ENTITY, DTO> {
    DTO toDTO(ENTITY entity);

    ENTITY toEntity(DTO dto);
}
