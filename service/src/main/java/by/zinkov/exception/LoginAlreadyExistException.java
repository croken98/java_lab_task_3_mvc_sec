package by.zinkov.exception;

public class LoginAlreadyExistException extends Exception {
    public LoginAlreadyExistException(String msg){
        super(msg);
    }
}
