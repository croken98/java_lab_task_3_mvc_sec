package by.zinkov.config;

import by.zinkov.convertors.EntityDtoConvertor;
import by.zinkov.repository.Repository;
import by.zinkov.repository.impl.CountryRepository;
import by.zinkov.service.Service;
import by.zinkov.service.impl.CountryService;
import by.zinkov.specification.impl.AllCountriesSpecification;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashMap;
import java.util.Map;

@EnableTransactionManagement
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan(basePackageClasses = {Repository.class
        , Service.class
        , AllCountriesSpecification.class
        , CountryService.class
        , CountryRepository.class
        , EntityDtoConvertor.class})
public class DBConfig {

    @Value("/hicaricp.properties")
    private String pathToProperty;

    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(entityManagerFactory());
        return manager;
    }

    @Bean
    public DataSource hikariDataSource() {
        HikariConfig config = new HikariConfig(pathToProperty);
        config.setMaximumPoolSize(20);
        return new HikariDataSource(config);
    }

    @Bean
    public EntityManager entityManager() {
        return entityManagerFactory().createEntityManager();
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        Map<String, Object> addedOrOverridenProperties = new HashMap<>();
        addedOrOverridenProperties.put("hibernate.connection.datasource", hikariDataSource());
        //addedOrOverridenProperties.put("hibernate.jdbc.batch_size", "10");
        return Persistence.createEntityManagerFactory("persistence_unit_one", addedOrOverridenProperties);
    }

    @Bean
    public ValidatorFactory validatorFactory() {
        return Validation.buildDefaultValidatorFactory();
    }

    @Bean
    public Validator validator() {
        return validatorFactory().getValidator();
    }
}