CREATE TABLE IF NOT EXISTS persistent_logins (
  username  character varying(64) NOT NULL,
  series    character varying(64) NOT NULL,
  token     character varying(64) NOT NULL,
  last_used TIMESTAMP   NOT NULL,
  PRIMARY KEY (series)
);

create table user_travel_agency(
  id serial primary key,
  login character varying(45)  not null,
  password character(60)  not null,
  role character varying(45)  not null
);

create TYPE features_enum as ENUM(
  'FREE_WIFI',
  'WINE_BAR',
  'BILLIARD_TABLE',
  'INDOOR_HOTEL_POOL',
  'FITNESS_CENTER',
  'ROOFTOP_ABSEILING',
  'BUSINESS_SERVICES',
  'TRANSPORTATION_AND_PARKING',
  'FREE_DRINKS',
  'GUEST_SERVICES'
   );

create table user_tour(
  id serial primary key,
  user_id integer  not null,
  tour_id integer  not null
  );

create table review(
  id serial primary key  not null,
  date TIMESTAMP  not null,
  text CHARACTER VARYING(1000)  not null,
  user_id integer  not null,
  tour_id integer  not null
  );


create table country(
  id serial primary key  not null,
  name CHARACTER VARYING(40)  not null
  );


create table hotel(
  id serial primary key,
  name CHARACTER VARYING(45)  not null,
  stars smallint  not null,
  website character varying(100)  not null,
  lalitude character varying(25)  not null,
  longitude  character varying(25)  not null,
  features features_enum[]  not null
  );


create TYPE tour_type_enum as ENUM(
  'EVENT_TRAVEL',
  'VISITING_FRIENDS_OR_RELATIVES',
  'BUSINESS_TRAVEL',
  'THE_GAP_YEAR',
  'LONG_TERM_SLOW_TRAVEL',
  'VOLUNTEER_TRAVEL',
  'THE_CARAVAN_OR_RV_ROAD_TRIP',
  'THE_GROUP_TOUR',
  'THE_PACKAGE_HOLIDAY',
  'THE_WEEKEND_BREAK'
  );

create table tour(
  id serial primary key,
  photo character varying(450)  not null,
  date timestamp  not null,
  duration bigint  not null,
  description text  not null,
  cost decimal  not null,
  tour tour_type_enum  not null,
  hotel_id integer  not null,
  country_id integer  not null
  );


alter TABLE review
    ADD CONSTRAINT fk_reviews_user FOREIGN KEY (user_id) REFERENCES user_travel_agency (id);

alter TABLE review
    ADD CONSTRAINT fk_reviews_tour FOREIGN KEY (tour_id) REFERENCES tour(id);

alter TABLE user_tour
    ADD CONSTRAINT fk_user_tour_user FOREIGN KEY (user_id) REFERENCES user_travel_agency (id);


alter TABLE user_tour
    ADD CONSTRAINT fk_user_tour_tour FOREIGN KEY (tour_id) REFERENCES tour(id);

alter TABLE tour
    ADD CONSTRAINT fk_tour_country FOREIGN KEY (country_id) REFERENCES country(id);

alter TABLE tour
    ADD CONSTRAINT fk_tour_hotel FOREIGN KEY (hotel_id) REFERENCES hotel(id);